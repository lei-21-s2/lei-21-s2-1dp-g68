package app.adapter;

import app.algortim.SumApi;
import app.domain.model.Sum;

import java.io.Serializable;

public class SumAdpter implements Serializable, Sum {
    private final SumApi api;

    public SumAdpter() {
        this.api = new SumApi();
    }

    @Override
    public int[] Max(int[] seq) {
        return api.Max(seq);
    }
}
