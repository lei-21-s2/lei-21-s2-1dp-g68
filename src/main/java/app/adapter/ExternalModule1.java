package app.adapter;

import app.domain.model.Parameter;
import com.example2.EMRefValue;
import com.example3.CovidReferenceValues1API;
import net.sourceforge.barbecue.output.OutputException;

import java.io.IOException;
import java.util.Date;

public class ExternalModule1 extends CovidReferenceValues1API implements ReferenceValuesAPI {

    @Override
    public EMRefValue getReferenceValues(Parameter parameter) throws OutputException, IOException {
        Double min=this.getMinReferenceValue(parameter.getCode(),12345);
        Double max=this.getMaxReferenceValue(parameter.getCode(),12345);
        String metric=this.usedMetric(parameter.getCode(),12345);

        Date date=new Date();

        return new EMRefValue(parameter.getCode(),metric,min,max,date);
    }
}
