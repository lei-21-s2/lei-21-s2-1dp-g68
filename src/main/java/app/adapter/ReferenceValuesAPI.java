package app.adapter;

import app.domain.model.Parameter;
import com.example2.EMRefValue;
import net.sourceforge.barbecue.output.OutputException;

import java.io.IOException;

public interface ReferenceValuesAPI {

    EMRefValue getReferenceValues(Parameter parameter) throws OutputException, IOException;
}
