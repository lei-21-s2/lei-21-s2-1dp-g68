package app.adapter;

import app.domain.model.Parameter;
import com.example2.EMRefValue;
import com.example2.ExternalModule2API;
import net.sourceforge.barbecue.output.OutputException;

import java.io.IOException;

public class ExternalModule2 extends ExternalModule2API implements ReferenceValuesAPI {

    @Override
    public EMRefValue getReferenceValues(Parameter parameter) throws OutputException, IOException {
        return this.getReferenceFor(parameter.getCode());
    }
}
