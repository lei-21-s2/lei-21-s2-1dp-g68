package app.adapter;

import app.algortim.SearchAlgorithm;
import app.domain.model.SearchAlgoritmInt;
import app.domain.model.Test;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SearchAlgoritmAdpter implements Serializable, SearchAlgoritmInt {

    SearchAlgorithm searchAlgorithm;

    public SearchAlgoritmAdpter() {
        this.searchAlgorithm = new SearchAlgorithm();
    }

    @Override
    public Map<String, List<Integer>> getLIstbyBruteForceAlgorithm(Date beginDate, Date endDate, List<Test> testList) {
        return searchAlgorithm.getLIstbyBruteForceAlgorithm(beginDate, endDate, testList);
    }
}
