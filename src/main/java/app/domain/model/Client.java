package app.domain.model;

import app.domain.shared.Verifier;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Client {

	private String name;
	private String email;
	private String sex;
	private String address;
	private Date birthDate;
	private int citizenCardNumber;
	private long nhsnumber;
	private long tifnumber;
	private long phoneNumber;

	public Client ( ) {
	}

	public Client ( String name, String email, String address, String sex, String birthDate,
					String citizenCardNumber, String nhsnumber, String tifnumber, String phoneNumber ) {

		Date date;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		setName( name );
		setEmail( email );
		setSex( sex );
		setAdress( address );

		try {
			date = formatter.parse(birthDate);
			setBirthDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		setCitizenCardNumber(  Integer.parseInt(citizenCardNumber) );
		setNHSnumber( Long.parseLong(nhsnumber) );
		setTIFnumber( Long.parseLong(tifnumber) );
		try {
			setPhoneNumber(Long.parseLong(phoneNumber));
		}catch (Exception e){

		}

	}

	public Client ( String name, String email, String sex, Date birthDate,
					int citizenCardNumber, long nhsnumber, long tifnumber, long phoneNumber ) {
		setName( name );
		setEmail( email );
		setSex( sex );
		setBirthDate( birthDate );
		setCitizenCardNumber( citizenCardNumber );
		setNHSnumber( nhsnumber );
		setTIFnumber( tifnumber );
		try {
			setPhoneNumber(phoneNumber);
		}catch (Exception e){

		}
	}

	public String getName ( ) {
		return this.name;
	}

	public void setName ( String name ) {
		if ( Verifier.isNull( name ) ) {
			throw new IllegalArgumentException(  );
		} else {
			this.name = name;
		}
	}

	public String getEmail ( ) {
		return this.email;
	}

	public void setEmail ( String email ) {
		this.email = email;
	}

	public String getSex ( ) {
		return this.sex;
	}

	public void setSex ( String sex ) {
		this.sex = sex;
	}

	public String getAdress ( ) {
		return this.email;
	}

	public void setAdress ( String address ) {
		this.address = address;
	}

	public Date getBirthDate ( ) {
		return this.birthDate;
	}

	public void setBirthDate ( Date birthDate ) {
		this.birthDate = birthDate;
	}

	public int getCitizenCardNumber ( ) {
		return this.citizenCardNumber;
	}

	public void setCitizenCardNumber ( int citizenCardNumber ) {
		this.citizenCardNumber = citizenCardNumber;
	}

	public long getNHSnumber ( ) {
		return this.nhsnumber;
	}

	public void setNHSnumber ( long nhsnumber ) {
		this.nhsnumber = nhsnumber;
	}

	public long getTIFnumber ( ) {
		return this.tifnumber;
	}

	public void setTIFnumber ( long tifnumber ) {
		this.tifnumber = tifnumber;
	}

	public long getPhoneNumber ( ) {
		return this.phoneNumber;
	}

	public void setPhoneNumber ( long phoneNumber ) throws Exception {

		Pattern p = Pattern.compile("(9[1236][0-9]) ?([0-9]{3}) ?([0-9]{3})");

		// Pattern class contains matcher() method
		// to find matching between given number
		// and regular expression
		Matcher m = p.matcher(String.valueOf(phoneNumber));

		if(m.matches())
			this.phoneNumber = phoneNumber;
		else
			throw new Exception("Invalid phone number");


	}

	@Override
	public String toString() {
		return "Client{" +
				"name='" + name + '\'' +
				", email='" + email + '\'' +
				", address='" + address + '\'' +
				", sex='" + sex + '\'' +
				", birthDate=" + birthDate +
				", citizenCardNumber=" + citizenCardNumber +
				", NHS_number=" + nhsnumber +
				", TIF_number=" + tifnumber +
				", phoneNumber=" + phoneNumber +
				'}';
	}
}
