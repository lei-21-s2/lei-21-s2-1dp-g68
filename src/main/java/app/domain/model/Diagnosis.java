package app.domain.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Diagnosis {

    private String report;
    private LocalDate date;

    public Diagnosis(){}

    public Diagnosis(String report,LocalDate date){
        if(countWords(report)>400)
            throw new IllegalArgumentException("Report with more than 400 words");
        else
            this.report = report;
        this.date=date;
    }

    public String getReport() {
        return report;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setReport(String report) {
        if(countWords(report)>400)
            throw new IllegalArgumentException("Report with more than 400 words");
        else
            this.report = report;
    }

    static final int OUT = 0;
    static final int IN = 1;

    // returns number of words in str
    public int countWords(String str)
    {
        int state = OUT;
        int wc = 0;  // word count
        int i = 0;

        // Scan all characters one by one
        while (i < str.length())
        {
            // If next character is a separator, set the
            // state as OUT
            if (str.charAt(i) == ' ' || str.charAt(i) == '\n'
                    || str.charAt(i) == '\t')
                state = OUT;


                // If next character is not a word separator
                // and state is OUT, then set the state as IN
                // and increment word count
            else if (state == OUT)
            {
                state = IN;
                ++wc;
            }

            // Move to next character
            ++i;
        }
        return wc;
    }

    @Override
    public String toString() {
        return "Diagnosis{" +
                "report='" + report + '\'' +
                ", date=" + date +
                '}';
    }
}
