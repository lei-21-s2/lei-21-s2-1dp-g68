package app.domain.model;

import java.util.Date;

public class Sample {
    private Date sampleDate;
    private String barcode;

    public Sample(Date sampleDate, String barcode) {
        this.sampleDate = sampleDate;
        this.barcode = barcode;
    }

    public Date getSampleDate() {
        return sampleDate;
    }

    public void setSampleDate( Date sampleDate ) {
        this.sampleDate = sampleDate;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode( String barcode ) {
        this.barcode = barcode;
    }

    @Override
    public String toString() {
        return "Sample{" +
                "dateofsampletaken=" + sampleDate +
                ", barcode=" + barcode +
                '}';
    }
}
