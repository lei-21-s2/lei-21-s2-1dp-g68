package app.domain.model;

import java.util.ArrayList;

public interface SearchClientTIN {

    public ArrayList<Client> getListClientByTin(ArrayList<Client> clientArrayList);
}
