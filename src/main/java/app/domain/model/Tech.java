package app.domain.model;

public class Tech extends Employee {

	public Tech ( String employeeID, String name, String address, String phoneNumber, String email, String socCode ) {
		super( employeeID, name, address, phoneNumber, email, socCode );
	}

}
