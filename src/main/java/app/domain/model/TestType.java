package app.domain.model;

public class TestType {

	private String name;
	private String code;
	private String collectionMethod;
	private String module;

	public TestType ( String name, String code, String collectionMethod, String module ) {
		setName( name );
		setCode( code );
		setCollectionMethod( collectionMethod );
		setModule( module );
	}

	public TestType() {
	}

	public String getName ( ) {
		return name;
	}

	public void setName ( String name ) {
		this.name = name;
	}

	public String getCode ( ) {
		return code;
	}

	public void setCode ( String code ) {
		this.code = code;
	}

	public String getCollectionMethod ( ) {
		return collectionMethod;
	}

	public void setCollectionMethod ( String collectionMethod ) {
		this.collectionMethod = collectionMethod;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@Override
	public String toString() {
		return "TestType{" +
				"name='" + name + '\'' +
				"code= "+ code+ '\'' +
				"code='" + code + '\'' +
				"collectionMethod='" + collectionMethod + '\'' +
				"module='" + module + '\'' +
				'}';
	}
}
