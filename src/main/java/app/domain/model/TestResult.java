package app.domain.model;

import com.example2.EMRefValue;

import java.time.LocalDate;
import java.util.Date;

public class TestResult {

    private LocalDate trDate;
    private String result;
    private String metric;
    private EMRefValue values;

    public TestResult(){}

    public TestResult(LocalDate trDate, String result, String metric, EMRefValue ref){
        this.trDate=trDate;
        this.result=result;
        this.metric=metric;
        this.values=ref;
    }

    public TestResult(LocalDate trDate) {
        this.trDate = trDate;
    }

    public LocalDate getTrDate() {
        return trDate;
    }

    public void setTrDate(LocalDate trDate) {
        this.trDate = trDate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public EMRefValue getValues() {
        return values;
    }

    public void setValues(EMRefValue values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "Test Result Date=" + trDate +
                ", result='" + result + '\'' +
                ", metric='" + metric + '\'' +
                '}';
    }
}
