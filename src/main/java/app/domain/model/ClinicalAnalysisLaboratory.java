package app.domain.model;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Set;

public class ClinicalAnalysisLaboratory {

	private int laboratoryId;
	private String name;
	private String address;
	private String phoneNumber;
	private int tinNumber;
	private List<TestType> testtype;

	public ClinicalAnalysisLaboratory ( int laboratoryId, String name, String address, String phoneNumber,
										int tinNumber, List<TestType> testType ) {
		checkLaboratoryIdRules( laboratoryId );
		checkNameRules( name );
		this.laboratoryId = laboratoryId;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.tinNumber = tinNumber;
		this.testtype = testType;
	}

	private void checkNameRules ( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			throw new IllegalArgumentException( "Name cannot be blank." );
		}
		if ( name.length( ) > 10 ) {
			throw new IllegalArgumentException( "Name must have more than 10 chars." );
		}
	}

	private void checkLaboratoryIdRules ( int laboratoryId ) {
		if ( laboratoryId < 0 ) {
			throw new IllegalArgumentException( "Laboratory id must be a positive number." );
		}
	}

	public int getLaboratoryId() {
		return laboratoryId;
	}

	public String getName() {
		return name;
	}

	public void setLaboratoryId(int laboratoryId) {
		this.laboratoryId = laboratoryId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getTinNumber() {
		return tinNumber;
	}

	public void setTinNumber(int tinNumber) {
		this.tinNumber = tinNumber;
	}

	public List<TestType> getTesttype() {
		return testtype;
	}

	public void setTesttype(List<TestType> testtype) {
		this.testtype = testtype;
	}
}
