package app.domain.model;

import java.util.ArrayList;

public interface SearchClientName {

    public ArrayList<Client> getListClientByName(ArrayList<Client> clientArrayList);
}
