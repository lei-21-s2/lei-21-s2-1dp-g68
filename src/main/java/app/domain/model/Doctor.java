package app.domain.model;


public class Doctor extends Employee {

	private String doctorIndexNumber;

	public Doctor ( String employeeID, String name, String address, String phoneNumber,
					String email, String socCode, String doctorIndexNumber ) {
		super( employeeID, name, address, phoneNumber, email, socCode );
		setDoctorIndexNumber( doctorIndexNumber );
	}
	public String getDoctorIndexNumber ( ) {
		return doctorIndexNumber;
	}

	public void setDoctorIndexNumber ( String doctorIndexNumber ) {
		this.doctorIndexNumber = doctorIndexNumber;
	}
}
