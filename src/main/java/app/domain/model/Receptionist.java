package app.domain.model;

public class Receptionist extends Employee {

	public Receptionist ( String employeeID, String name, String address,
						  String phoneNumber, String email, String socCode ) {
		super( employeeID, name, address, phoneNumber, email, socCode );
	}
}
