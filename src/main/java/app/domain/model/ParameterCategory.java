package app.domain.model;

import org.apache.commons.lang3.StringUtils;

public class ParameterCategory {

	private String code;
	private String description;
	private String nhsId;
	private TestType testType;

	public ParameterCategory ( String code, String description, String nhsId, TestType testType ) {
		checkCodeRules( code );
		checkDescriptionRules( description );
		this.code = code;
		this.description = description;
		this.nhsId = nhsId;
		this.testType = testType;
	}

	private void checkCodeRules ( String code ) {
		if ( StringUtils.isBlank( code ) ) {
			throw new IllegalArgumentException( "Code cannot be blank." );
		}

		if ( ( code.length( ) < 4 ) || ( code.length( ) > 8 ) ) {
			throw new IllegalArgumentException( "Code must have 4 to 8 chars." );
		}
	}

	private void checkDescriptionRules ( String description ) {
		//mete empty please check
	}

	public String getCode ( ) {
		return code;
	}

	public void setCode ( String code ) {
		this.code = code;
	}

	public String getDescription ( ) {
		return description;
	}

	public void setDescription ( String description ) {
		this.description = description;
	}

	public String getNhsId ( ) {
		return nhsId;
	}

	public void setNhsId ( String nhsId ) {
		this.nhsId = nhsId;
	}

	public TestType getTestType() {
		return testType;
	}

	public void setTestType(TestType testType) {
		this.testType = testType;
	}

	@Override
	public String toString() {
		return "ParameterCategory{" +
				"code='" + code + '\'' +
				", description='" + description + '\'' +
				", nhsId='" + nhsId + '\'' +
				", testType=" + testType +
				'}';
	}
}
