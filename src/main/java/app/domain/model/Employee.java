package app.domain.model;

import auth.domain.model.Email;

public class Employee {

	private String employeeID;
	private String name;
	private String address;
	private String phoneNumber;
	private Email email;
	private String socCode;

	public Employee ( String employeeID, String name, String address, String phoneNumber, String email, String socCode ) {
		if ( employeeID == null || name == null || email == null || address == null ||
				phoneNumber == null || socCode == null ) {
			throw new NullPointerException( "Parameters cannot be null" );
		}

		setEmployeeID( employeeID );
		setName( name );
		setAddress( address );
		setPhoneNumber( phoneNumber );
		setEmail( email );
		setSocCode( socCode );
	}

	public String getEmployeeID ( ) {
		return employeeID;
	}

	public void setEmployeeID ( String employeeID ) {
		this.employeeID = employeeID;
	}

	public String getName ( ) {
		return name;
	}

	public void setName ( String name ) {
		if ( name.length( ) > 35 ) {
			throw new IllegalArgumentException( "The Employee name cant be more then 35 caracteres" );
		}

		this.name = name;
	}

	public String getAddress ( ) {
		return address;
	}

	public void setAddress ( String address ) {
		this.address = address;
	}

	public String getPhoneNumber ( ) {
		return phoneNumber;
	}

	public void setPhoneNumber ( String phoneNumber ) {
		if ( phoneNumber.length( ) != 9 ) {
			throw new IllegalArgumentException( "The number cant be different then 9 numbers" );
		}

		this.phoneNumber = phoneNumber;
	}

	public String getEmail ( ) {
		return email.getEmail( );
	}

	public void setEmail ( String email ) {
		this.email = new Email( email );
	}

	public String getSocCode ( ) {
		return socCode;
	}

	public void setSocCode ( String socCode ) {
		this.socCode = socCode;
	}

}
