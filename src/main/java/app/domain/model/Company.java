package app.domain.model;

import app.adapter.SearchAlgoritmAdpter;
import app.adapter.SumAdpter;
import auth.AuthFacade;
import auth.UserSession;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Company {

	private String designation;
	private final AuthFacade authFacade;
	private final Set< Client > clientList;
	private final Set< Receptionist > receptionistList;
	private final Set< Doctor > doctorList;
	private final Set< SpecialistDoctor > specialistDoctorList;
	private final Set< LaboratoryCoordinator > laboratoryCoordinatorList;
	private final Set< ClinicalChemistryTechnologist > clinicalChemistryTechnologists;
	private final Set< ClinicalAnalysisLaboratory > clinicalAnalysisLaboratoryList;
	private final Set< TestType > testTypeList;
	private final Set< ParameterCategory > parameterCategoryList;
	private final Set< Parameter > parameterList;
	private final List< Test > testList;
	private SearchAlgoritmInt searchAlgoritmInt;
	private Sum sum;
	public Company ( String designation , String sum , String serchAlgoritm) {

		if ( StringUtils.isBlank( designation ) ) {
			throw new IllegalArgumentException( "Designation cannot be blank." );
		}
		try {
			Class<?> sumClass=Class.forName(sum);
			this.sum=((Sum)sumClass.newInstance() );
			Class<?> searchClass=Class.forName(serchAlgoritm);
			this.searchAlgoritmInt=((SearchAlgoritmInt) searchClass.newInstance());
		}catch (Exception e){
			this.sum=new SumAdpter();
			this.searchAlgoritmInt=new SearchAlgoritmAdpter();
		}
		this.designation = designation;
		this.doctorList = new HashSet<>( );
		this.laboratoryCoordinatorList = new HashSet<>( );
		this.clientList = new HashSet<>( );
		this.receptionistList = new HashSet<>( );
		this.clinicalAnalysisLaboratoryList = new HashSet<>( );
		this.testTypeList = new HashSet<>( );
		this.parameterCategoryList = new HashSet<>( );
		this.parameterList = new HashSet<>( );
		this.testList = new ArrayList<>( );
		this.authFacade = new AuthFacade( );
		this.specialistDoctorList = new HashSet<>( );
		this.clinicalChemistryTechnologists=new HashSet<>();
	}

	public Company() {
		this.doctorList = new HashSet<>( );
		this.laboratoryCoordinatorList = new HashSet<>( );
		this.clientList = new HashSet<>( );
		this.receptionistList = new HashSet<>( );
		this.clinicalAnalysisLaboratoryList = new HashSet<>( );
		this.testTypeList = new HashSet<>( );
		this.parameterCategoryList = new HashSet<>( );
		this.parameterList = new HashSet<>( );
		this.testList = new ArrayList<>( );
		this.authFacade = new AuthFacade( );
		this.specialistDoctorList = new HashSet<>( );
		this.clinicalChemistryTechnologists=new HashSet<>();
	}

	public String getDesignation ( ) {

		return designation;
	}

	public AuthFacade getAuthFacade ( ) {

		return authFacade;
	}

	public boolean addClient ( Client client ) {

		return clientList.add( client );
	}

	public boolean addDoctor ( Doctor doctor ) {

		return doctorList.add( doctor );
	}

	public boolean addSpecialistDoctor ( SpecialistDoctor doctor ) {

		return specialistDoctorList.add( doctor );
	}

	public boolean addReceptionist ( Receptionist receptionist ) {

		return receptionistList.add( receptionist );
	}

	public boolean addLaboratoryCoordinator ( LaboratoryCoordinator coordinator ) {

		return laboratoryCoordinatorList.add( coordinator );
	}

	public boolean addClinicalChemistryTechnologist ( ClinicalChemistryTechnologist clinicalChemistryTechnologist ) {

		return this.clinicalChemistryTechnologists.add( clinicalChemistryTechnologist );
	}

	public boolean addClinicalAnalysisLaboratory ( ClinicalAnalysisLaboratory clinicalAnalysisLaboratory ) {

		return this.clinicalAnalysisLaboratoryList.add( clinicalAnalysisLaboratory );
	}

	public boolean addTypeTest ( TestType testType ) {

		return this.testTypeList.add( testType );
	}

	public List< TestType > getTestTypeList ( ) {

		return new ArrayList<>( this.testTypeList );
	}

	public boolean addParameterCategory ( ParameterCategory parameterCategory ) {

		return this.parameterCategoryList.add( parameterCategory );
	}

	public List< ParameterCategory > getParameterCategoryList ( ) {

		return new ArrayList<>( this.parameterCategoryList );
	}

	public boolean addParameter ( Parameter parameter ) {

		return this.parameterList.add( parameter );
	}

	public boolean addTest ( Test test ) {

		return this.testList.add( test );
	}

	public Set< Client > getClientList ( ) {

		return clientList;
	}

	public Set< TestType > getTypeTestList ( ) {

		return testTypeList;
	}

	public Set< Parameter > getParameterList ( ) {

		return parameterList;
	}

	public List< Test > getTestList ( ) {

		return testList;
	}

	public boolean addSample ( String testCode, String clientEmail, Sample sample ) {

		int position = -1;
		for ( int i = 0; i < testList.size( ); i++ ) {
			if ( testList.get( i ).getInternalCode( ).equals( testCode ) && testList.get( i ).getClient( ).getEmail( ).equals( clientEmail ) ) {
				position = i;
				break;
			}
		}
		return testList.get( position ).getSampleList( ).add( sample );
	}
	public List<Test> getTestbetweenDates(Date date1,Date date2 ){
		List<Test> testList11=new LinkedList<>();
		for (int i = 0; i < testList.size(); i++) {
			if (date1.before(testList.get(i).getDateofcriaction()) || date2.after(testList.get(i).getDateofcriaction())){
				testList11.add(testList.get(i));
			}
		}
		return testList11;
	}
	public Sum getSum() {
		return sum;
	}

	public Test getTestByCode(String internalCode){
		for(Test t : testList){
			if(t.getInternalCode().equals(internalCode)){
				return t;
			}
		}
		return null;
	}

	public TestType getTestTypeByCode(String code){
		for(TestType tt : testTypeList){
			if(tt.getCode().equals(code)){
				return tt;
			}
		}
		return null;
	}

	public boolean editClient(Client client,int pos){

		int result = 0;
		for (Object entry : clientList) {
			if(result==pos){
				entry=client;
			}
			result++;
		}
		return clientList.contains(client);
	}

	public Client getClientByEmail(String email) {
		for (Client c : clientList) {
			if (c.getEmail().equals(email)) {
				return c;
			}
		}
		return null;
	}

	public ArrayList<Test> listTestClient(UserSession client){
		ArrayList<Test> cTests =  new ArrayList<Test>();
		List<Test> listTest = getTestList();

		for(Test t: listTest){
			if(t.getClient().getEmail().equals(client.getUserId().toString())){
				cTests.add( t);
			}
		}
		Collections.sort(cTests,
		(o1, o2) -> o2.getDateofcriaction().compareTo(o1.getDateofcriaction()));

		return cTests;
	}

	public ArrayList<Test> listTestClient1(Client client){
		ArrayList<Test> cTests =  new ArrayList<Test>();
		List<Test> listTest = getTestList();

		for(Test t: listTest){
			if(t.getClient().getEmail().equals(client.getEmail())){
				cTests.add( t);
			}
		}
		Collections.sort(cTests,
				(o1, o2) -> o2.getDateofcriaction().compareTo(o1.getDateofcriaction()));

		return cTests;
	}

	public SearchAlgoritmInt getSearchAlgoritmInt() {
		return searchAlgoritmInt;
	}

	public ArrayList<Client> clientList(){
		ArrayList<Client> clients=new ArrayList<>();
		List<Test> testList=getTestList();

		for(Test t : testList){
			if(t.getValidationDate()!=null && !clients.contains(t.getClient())){
				clients.add(t.getClient());
			}
		}

		Collections.sort(clients,
				(o1, o2) -> o2.getName().compareTo(o1.getName()));

		return clients;
	}
}
