package app.domain.model;

import com.example2.EMRefValue;

import java.time.LocalDate;
import java.util.Date;

public class Parameter {


	private String code;
	private String name;
	private String shortDescritpion;
	private ParameterCategory category;
	private TestResult testResult;

	public Parameter ( String code, String name, String shortDescritpion, ParameterCategory category ) {
		setCode( code );
		setName( name );
		setShortDescritpion( shortDescritpion );
		setCategory( category );
	}


	public String getCode ( ) {
		return code;
	}

	public void setCode ( String code ) {
		if ( code.length() != 5 ) {
			throw new IllegalArgumentException( "Code length must be 5." );
		} else {
			this.code = code;
		}
	}

	public String getName ( ) {
		return name;
	}

	public void setName ( String name ) {
		if ( name.length() > 8 ) {
			throw new IllegalArgumentException( "Name length must be smaller than 8." );
		} else {
			this.name = name;
		}
	}

	public String getShortDescritpion ( ) {
		return shortDescritpion;
	}

	public void setShortDescritpion ( String shortDescritpion ) {
		if ( shortDescritpion.length() > 20 && shortDescritpion.matches( "[a-zA-Z]" ) ) {
			throw new IllegalArgumentException( "Short description length must be smaller than 20 and a string." );
		} else {
			this.shortDescritpion = shortDescritpion;
		}
	}

	public ParameterCategory getCategory ( ) {
		return category;
	}

	public void setCategory ( ParameterCategory category ) {
		this.category = category;
	}

	public boolean addTestResult(LocalDate date, String result, String metric, EMRefValue ref){
		this.testResult=new TestResult(date,result,metric,ref);
		return true;
	}

	public TestResult getTestResult() {
		return testResult;
	}

	public void setTestResult(TestResult testResult) {
		this.testResult = testResult;
	}

	@Override
	public String toString() {
		return "Parameter{" +
				"code='" + code + '\'' +
				", name='" + name + '\'' +
				", shortDescritpion='" + shortDescritpion + '\'' +
				", category=" + category +
				'}';
	}
}
