package app.domain.model;

public class LaboratoryCoordinator extends Employee {

    private String coordinatorIndexNumber;

    public LaboratoryCoordinator(String employeeID,String name,String address,String phoneNumber,String email,String socCode,String coordinatorIndexNumber){
        super(employeeID,name,address,phoneNumber,email,socCode);
        setCoordinatorIndexNumber(coordinatorIndexNumber);
    }

    public String getCoordinatorIndexNumber() {
        return coordinatorIndexNumber;
    }

    public void setCoordinatorIndexNumber(String coordinatorIndexNumber) {
        this.coordinatorIndexNumber=coordinatorIndexNumber;
    }

}
