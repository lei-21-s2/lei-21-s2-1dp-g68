package app.domain.model;

public class ClinicalChemistryTechnologist extends Employee {

	public ClinicalChemistryTechnologist ( String employeeID, String name, String address,
										   String phoneNumber, String email, String socCode ) {
		super( employeeID, name, address, phoneNumber, email, socCode );
	}

}
