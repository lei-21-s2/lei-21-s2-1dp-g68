package app.domain.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SearchAlgoritmInt {

    public Map<String, List<Integer>> getLIstbyBruteForceAlgorithm(Date beginDate, Date endDate, List<Test> testList);
}
