package app.domain.model;

import java.util.*;

public class Test {

    private String internalCode;
    private String nhsCode;
    private TestType type;
    private Set<ParameterCategory> pcList;
    private Set<Parameter> pList;
    private TestResult testResult;
    private Client client;
    private List<Sample> sampleList;
    private Diagnosis diagnosis;
    private Date dateofcriaction;
    private Date validationDate;

    public Test(){}

    public Test(String internalCode, String nhsCode,TestType type, Set<ParameterCategory> pcList,
                Set<Parameter> pList, Client client) {

        setInternalCode(internalCode);
        setNhsCode(nhsCode);
        setType(type);
        setPcList(pcList);
        setpList(pList);
        setClient(client);
        sampleList=new ArrayList<>();
        dateofcriaction=new Date();
        testResult=new TestResult();
        diagnosis=new Diagnosis();
        validationDate=null;

    }

    public Test(String internalCode, String nhsCode, TestType type, Set<ParameterCategory> pcList, Set<Parameter> pList, TestResult testResult, Client client, List<Sample> sampleList, Diagnosis diagnosis, Date dateofcriaction, Date validationDate) {
        this.internalCode = internalCode;
        this.nhsCode = nhsCode;
        this.type = type;
        this.pcList = pcList;
        this.pList = pList;
        this.testResult = testResult;
        this.client = client;
        this.sampleList = sampleList;
        this.diagnosis = diagnosis;
        this.dateofcriaction = dateofcriaction;
        this.validationDate = validationDate;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        if(internalCode.length() != 12){
            throw new IllegalArgumentException("Internal Code length must be 12");
        } else {
            this.internalCode = internalCode;
        }
    }

    public String getNhsCode() {
        return nhsCode;
    }

    public void setNhsCode(String nhsCode) {
        if(nhsCode.length() != 12 && nhsCode.matches("^[a-zA-Z0-9]*$")){
            throw new IllegalArgumentException("NHS Code must be 12 and alphanumeric");
        }else{
            this.nhsCode = nhsCode;
        }

    }

    public TestType getType() {
        return type;
    }

    public void setType(TestType type) {
        if(type == null){
            throw new IllegalArgumentException("Test type cant be null");
        }else{
            this.type = type;
        }

    }

    public Set<ParameterCategory> getPcList() {
        return pcList;
    }

    public void setPcList(Set<ParameterCategory> pcList) {
        if(pcList.isEmpty()){
            throw new IllegalArgumentException("List of Parameter Category cant be empty");
        }else {
            this.pcList = pcList;
        }

    }

    public Set<Parameter> getpList() {
        return pList;
    }

    public void setpList(Set<Parameter> pList) {
        if(pList.isEmpty()){
            throw new IllegalArgumentException("List of Parameter cant empty");
        }else {
            this.pList = pList;
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        if(client == null){
            throw new IllegalArgumentException("Client cant be null");
        }else{
            this.client = client;
        }
    }

    public List<Sample> getSampleList() {
        return sampleList;
    }

    public void setSampleList(List<Sample> sampleList) {
        this.sampleList = sampleList;
    }

    public Diagnosis getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(Diagnosis diagnosis) {
        if(diagnosis==null)
            throw new IllegalArgumentException("Diagnosis can't be null");
        else
            this.diagnosis = diagnosis;
    }


    public Date getDateofcriaction() {
        return dateofcriaction;
    }

    public void setDateofcriaction(Date dateofcriaction) {
        this.dateofcriaction = dateofcriaction;
    }

    @Override
    public String toString() {
        return "Test "+ getInternalCode() + " " + getDateofcriaction().toString();
    }


    public String print(){
        return "Test:" + getInternalCode() + "\n" + getNhsCode() + "\n" + getType() + "\n" + getPcList() +
                "\n" + getpList() + "\n" + getTestResult() + "\n" + getSampleList() + "\n" + getDiagnosis() +
                "\n" + getDateofcriaction() + "\n" + getValidationDate() ;
    }


    public TestResult getTestResult() {
        return testResult;
    }

    public void setTestResult(TestResult testResult) {
        if(testResult==null){
            throw new IllegalArgumentException("testResult cant be null");
        }else{
            this.testResult = testResult;
        }

    }

    public Date getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Date validationDate) {
        this.validationDate = validationDate;

    }
    public Parameter getTestForParameter(String parameterCode){
        for(Parameter p : pList){
            if(p.getCode().equals(parameterCode)){
                return p;
            }
        }
        throw new NullPointerException("No parameter with that code");
    }


}