package app.domain.shared;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Constants {
    public static final String ROLE_ADMIN = "ADMINISTRATOR";
    public static final String ROLE_DOCTOR = "DOCTOR";
    public static final String ROLE_SPECIALIST_DOCTOR = "SPECIALIST_DOCTOR";
    public static final String ROLE_CLIENT = "CLIENT";
    public static final String ROLE_RECEPTIONIST = "REPEPTIONIST";
    public static final String ROLE_LABORATORY_COORDINATOR = "LABORATORY_COORDINATOR";
    public static final String ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST = "CLINICAL_CHEMISTRY_TECHNOLOGIST";
    public static final String PARAMS_FILENAME = "config.properties";
    public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";

    public static final String TEST_TYPE_COVID = "SARS-COV2";
    public static final String TEST_TYPE_BLOOD = "BLOOD";
}
