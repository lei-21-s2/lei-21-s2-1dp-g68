package app.searchClient;

import app.domain.model.Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SearchClientByTIN implements Serializable{

    public SearchClientByTIN() {
    }

    public ArrayList<Client> getListClientByTin(ArrayList<Client> clientArrayList) {
        ArrayList<Client> clientArrayListNova = clientArrayList;

        Collections.sort(clientArrayListNova, new Comparator<Client>() {
            @Override
            public int compare(Client o1, Client o2) {
                return Long.compare(o1.getTIFnumber(), o2.getTIFnumber());
            }
        });

        return clientArrayListNova;
    }
}

