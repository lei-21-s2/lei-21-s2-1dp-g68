package app.searchClient;

import app.domain.model.Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SearchClientByName implements Serializable {

    public SearchClientByName() {
    }

    public ArrayList<Client> getListClientByName(ArrayList<Client> clientArrayList) {
        ArrayList<Client> clientArrayListNova = clientArrayList;

        Collections.sort(clientArrayListNova,
                Comparator.comparing(Client::getName));

        return clientArrayListNova;
    }
}





