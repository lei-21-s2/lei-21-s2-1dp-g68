package app.controller.register;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.shared.Constants;

public class RegisterClientController {

	private final Company company;

	public RegisterClientController ( ) {
		this.company = App.getInstance( ).getCompany( );
	}

	public boolean registerClient ( Client client, String password ) {
		boolean wasAdded = false;

		if ( !company.getAuthFacade().existsUser( client.getEmail() ) ) {
			 wasAdded = company.addClient( client ) &&
					 company.getAuthFacade()
							.addUserWithRole(
									client.getName(),
									client.getEmail(),
									password,
									Constants.ROLE_CLIENT
							);
		}

		return wasAdded;

	}

}
