package app.controller.register;

import app.controller.App;
import app.domain.model.ClinicalChemistryTechnologist;
import app.domain.model.Company;
import app.domain.shared.Constants;

public class RegisterClinicalChemistryTechnologistController {

	private final Company company;

	public RegisterClinicalChemistryTechnologistController ( ) {
		this.company = App.getInstance( ).getCompany( );
	}

	public boolean registerClinicalChemistryTechnologist ( ClinicalChemistryTechnologist clinicalChemistryTechnologist,
														   String password ) {
		boolean wasAdded = false;

		if ( !company.getAuthFacade().existsUser( clinicalChemistryTechnologist.getEmail() ) ) {
			wasAdded = company.addClinicalChemistryTechnologist( clinicalChemistryTechnologist ) &&
					company.getAuthFacade()
							.addUserWithRole(
									clinicalChemistryTechnologist.getName(),
									clinicalChemistryTechnologist.getEmail(),
									password,
									Constants.ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST
							);
		}

		return wasAdded;

	}
}
