package app.controller.register;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Doctor;
import app.domain.shared.Constants;

public class RegisterDoctorController {

	private Company company;

	public RegisterDoctorController ( ) {
		this.company = App.getInstance( ).getCompany();
	}

	public boolean registerDoctor( Doctor doctor, String password ) {
		boolean wasAdded = false;

		if ( !company.getAuthFacade().existsUser( doctor.getEmail() ) ) {
			wasAdded = company.addDoctor( doctor ) &&
					company.getAuthFacade()
							.addUserWithRole(
									doctor.getName(),
									doctor.getEmail(),
									password,
									Constants.ROLE_DOCTOR
							);
		}

		return wasAdded;
	}
}
