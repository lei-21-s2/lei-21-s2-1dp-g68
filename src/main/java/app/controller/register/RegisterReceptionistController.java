package app.controller.register;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Receptionist;
import app.domain.shared.Constants;

public class RegisterReceptionistController {

	private Company company;

	public RegisterReceptionistController ( ) {
		this.company = App.getInstance( ).getCompany( );
	}

	public boolean registerReceptionist ( Receptionist receptionist, String password ) {
		boolean wasAdded = false;

		if ( !company.getAuthFacade().existsUser( receptionist.getEmail() ) ) {
			wasAdded = company.addReceptionist( receptionist ) &&
					company.getAuthFacade()
							.addUserWithRole(
									receptionist.getName(),
									receptionist.getEmail(),
									password,
									Constants.ROLE_RECEPTIONIST
							);
		}

		return wasAdded;
	}

}
