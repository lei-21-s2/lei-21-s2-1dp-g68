package app.controller.register;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.LaboratoryCoordinator;
import app.domain.shared.Constants;

public class RegisterLaboratoryCoordinatorController {

    private Company company;

    public RegisterLaboratoryCoordinatorController() {
        this.company= App.getInstance().getCompany();
    }

    public boolean registerLaboratoryCoordinator(LaboratoryCoordinator coordinator, String password ) {
        boolean wasAdded = false;

        if(!company.getAuthFacade().existsUser(coordinator.getEmail())){
            wasAdded=company.addLaboratoryCoordinator(coordinator) && company.getAuthFacade().addUserWithRole(coordinator.getName(),coordinator.getEmail(),password, Constants.ROLE_LABORATORY_COORDINATOR);
        }

        return wasAdded;
    }

}
