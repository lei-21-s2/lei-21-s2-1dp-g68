package app.controller.roles;

import app.controller.register.RegisterClientController;
import app.domain.model.Client;

public class ReceptionistController {

	private final RegisterClientController registerClientController;

	public ReceptionistController ( ) {
		this.registerClientController = new RegisterClientController( );
	}

	public boolean registerClient ( Client client, String password ) {
		return this.registerClientController.registerClient( client, password );
	}

}
