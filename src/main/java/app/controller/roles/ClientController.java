package app.controller.roles;

import app.controller.App;
import app.controller.register.RegisterClientController;
import app.domain.model.Client;
import app.domain.model.Company;

public class ClientController {

    private Company company;
    private RegisterClientController registerClientController;

    public ClientController(){
        this.company= App.getInstance().getCompany();
        this.registerClientController=new RegisterClientController();
    }

    public boolean registerClient(Client client,String password){
        return this.registerClientController.registerClient(client,password);
    }

}
