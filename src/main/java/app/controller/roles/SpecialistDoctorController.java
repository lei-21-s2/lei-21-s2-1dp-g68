package app.controller.roles;

import app.controller.App;
import app.controller.register.RegisterSpecialistDoctorController;
import app.domain.model.Company;
import app.domain.model.SpecialistDoctor;

public class SpecialistDoctorController {

    private Company company;
    private RegisterSpecialistDoctorController registerSpecialistDoctorController;

    public SpecialistDoctorController(){
        this.company=App.getInstance().getCompany();
        this.registerSpecialistDoctorController=new RegisterSpecialistDoctorController();
    }

    public boolean registerSpecialistDoctor(SpecialistDoctor specialistDoctor,String password){
        return this.registerSpecialistDoctorController.registerSpecialistDoctor(specialistDoctor,password);
    }

}
