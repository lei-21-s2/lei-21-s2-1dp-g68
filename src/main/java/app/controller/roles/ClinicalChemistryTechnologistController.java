package app.controller.roles;

import app.controller.App;
import app.controller.register.RegisterClinicalChemistryTechnologistController;
import app.domain.model.ClinicalChemistryTechnologist;
import app.domain.model.Company;

public class ClinicalChemistryTechnologistController {

	private Company company;
	private RegisterClinicalChemistryTechnologistController registerClinicalChemistryTechnologistController;

	public ClinicalChemistryTechnologistController ( ) {
		this.company = App.getInstance( ).getCompany( );
		this.registerClinicalChemistryTechnologistController = new RegisterClinicalChemistryTechnologistController( );
	}

	public boolean registerClinicalChemistryTechnologist ( ClinicalChemistryTechnologist clinicalChemistryTechnologist,
														   String password ) {
		return this.registerClinicalChemistryTechnologistController.registerClinicalChemistryTechnologist( clinicalChemistryTechnologist,
				password
		);
	}
}
