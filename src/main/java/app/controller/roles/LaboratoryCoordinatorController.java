package app.controller.roles;

import app.controller.App;
import app.controller.register.RegisterLaboratoryCoordinatorController;
import app.domain.model.Company;
import app.domain.model.LaboratoryCoordinator;

public class LaboratoryCoordinatorController {

    private Company company;
    private RegisterLaboratoryCoordinatorController registerLaboratoryCoordinatorController;

    public LaboratoryCoordinatorController(){
        this.company= App.getInstance().getCompany();
        this.registerLaboratoryCoordinatorController=new RegisterLaboratoryCoordinatorController();
    }

    public boolean registerLaboratoryCoordinator(LaboratoryCoordinator coordinator, String password){
        return this.registerLaboratoryCoordinatorController.registerLaboratoryCoordinator(coordinator,password);
    }

}
