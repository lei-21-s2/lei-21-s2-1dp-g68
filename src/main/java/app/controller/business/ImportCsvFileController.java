package app.controller.business;

import app.controller.App;
import app.domain.model.*;
import app.ui.console.business.ImportCsvFileUI;
import com.example2.EMRefValue;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ImportCsvFileController {

    private final Company company;
    private ImportCsvFileUI importCsvFileUI;

    public ImportCsvFileController(){
        this.importCsvFileUI = new ImportCsvFileUI();
        this.company = App.getInstance( ).getCompany( );
    }

    public boolean importCsvFile(String file) throws IOException{
        String line = "";
        String splitBy = ";";
        Client client;
        TestType testType;
        ParameterCategory parameterCategory = null;
        Parameter parameter;
        LocalDate localDate;
        Date date = null;
        Date dateCreation = null;
        Date dateValidation = null;
        TestResult testResult;
        Diagnosis diagnosis;
        Sample sample;
        List<Sample> sampleList;
        Test test;

        try {
            //parsing a CSV file into BufferedReader class constructor
            BufferedReader br = new BufferedReader(new FileReader(file));
            br.readLine();

            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] info = line.split(splitBy); // use comma as separator

                //client
                client = new Client(info[8], info[9], info[10],"M/F", info[6], info[3], info[4], info[5], info[7]);
                company.addClient(client);
                //importCsvFileUI.showClient(client);

                // test type
                if(info[11].equals("Covid")){
                    testType = new TestType("covid test", info[0], "nose swab/throat swab", "app.adapter.ExternalModule1");
                } else {
                    testType = new TestType("blood test", info[0], "arterial sampling/venipuncture sampling", "app.adapter.ExternalModule2");
                }
                company.addTypeTest(testType);
                //importCsvFileUI.showTestType(testType);

                // parameter category
                if(( info[12].length( ) > 4 ) && ( info[12].length( ) < 8 )){
                    parameterCategory = new ParameterCategory(info[12], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[13].length( ) > 4 ) && ( info[13].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[13], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[14].length( ) > 4 ) && ( info[14].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[14], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[15].length( ) > 4 ) && ( info[15].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[15], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[16].length( ) > 4 ) && ( info[16].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[16], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[17].length( ) > 4 ) && ( info[17].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[17], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[18].length( ) > 4 ) && ( info[18].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[18], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                if(( info[19].length( ) > 4 ) && ( info[19].length( ) < 8 )) {
                    parameterCategory = new ParameterCategory(info[19], "description", info[1], testType);
                    company.addParameterCategory(parameterCategory);
                    //importCsvFileUI.showParameterCategory(parameterCategory);
                }

                // parameter
                //Parameter parameter = new Parameter(info[0], info[11], "short description", parameterCategory,  );

                // test result
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

                try {
                    date = formatter.parse(info[22]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
                localDate = LocalDate.parse(info[22], df);

                EMRefValue ref = new EMRefValue("123","NA",2.0,3.0, date);
                testResult = new TestResult(localDate, "NA", "NA", ref);

                // diagnosis
                localDate = LocalDate.parse(info[23], df);
                diagnosis = new Diagnosis("report", localDate);

                // sample and date creation
                try {
                    date = formatter.parse(info[21]);
                    dateCreation = date;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sampleList = new ArrayList<>();

                sample = new Sample(date,"12345678");
                sampleList.add(sample);


                // date validation
                try {
                    dateValidation = formatter.parse(info[24]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // test
                List<ParameterCategory> list = company.getParameterCategoryList();
                Set<ParameterCategory> set = new HashSet<>(list);

                test = new Test(info[0], info[1], testType, set, company.getParameterList(), testResult, client, sampleList, diagnosis, dateCreation, dateValidation);
                importCsvFileUI.showTest(test, parameterCategory);
                company.addTest(test);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
