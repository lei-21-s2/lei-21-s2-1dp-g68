package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Sample;
import app.domain.model.Test;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;


public class CreateSampleController {

	private Company company;

	public CreateSampleController ( ) {

		this.company = App.getInstance( ).getCompany( );
	}

	public String createSample ( String testCode, String clientemail ) {

		List< Test > testList = company.getTestList( );
		int position = -1;
		for ( int i = 0; i < testList.size( ); i++ ) {
			if ( testList.get( i ).getInternalCode( ).equals( testCode ) && testList.get( i ).getClient( ).getEmail( ).equals( clientemail ) ) {
				position = i;
				break;
			}
		}
		Sample sample = new Sample( new Date( ), testCode + "_" + testList.get( position ).getSampleList( ).size( ) + 1 );
		this.company.addSample( testCode, clientemail, sample );
		//createBarcode( testCode + ( testList.get( position ).getSampleList( ).size( ) + 1 ) );
		return createBarcode( testCode + ( testList.get( position ).getSampleList( ).size( ) + 1 ) )+"\\"+testCode + ( testList.get( position ).getSampleList( ).size( ) + 1) ;
	}

	public String createBarcode ( String info ) {

		String path = "barcodeRepository\\";
		try {
			Barcode barcode = BarcodeFactory.createCodabar( info );
			barcode.setBarWidth( 1 );
			barcode.setPreferredBarHeight( 50 );
			barcode.setFont( new Font( Font.SANS_SERIF, Font.PLAIN, 10 ) );
			File file=new File(path+ info + ".JPEG");

			FileOutputStream fOS = new FileOutputStream( file );
			BarcodeImageHandler.writeJPEG( barcode, fOS );
			fOS.close( );
		} catch ( BarcodeException | OutputException | IOException e ) {
			e.printStackTrace( );
		}
		return path;
	}

	public List< Test > testList ( ) {

		return this.company.getTestList( );
	}

}
