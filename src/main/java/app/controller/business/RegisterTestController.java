package app.controller.business;

import app.controller.App;
import app.domain.model.*;


public class RegisterTestController {

    private Company company;

    public RegisterTestController() {
        this.company = App.getInstance().getCompany();
    }

    public boolean registerTest( Test test ){
        return this.company.addTest(test);
    }
}
