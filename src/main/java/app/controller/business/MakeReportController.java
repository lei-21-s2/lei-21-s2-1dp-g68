package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Diagnosis;
import app.domain.model.Test;

import java.time.LocalDate;
import java.util.List;

public class MakeReportController {

    private Company company;

    public MakeReportController() {
        this.company=App.getInstance().getCompany();
    }

    public boolean makeReport(String clientemail,String report){

        List<Test> testList=company.getTestList();
        Test test=new Test();

        for(Test t : testList){
            if(t.getClient().getEmail().equals(clientemail)){
                test=t;
            }
        }

        LocalDate date = LocalDate.now();

        Diagnosis diagnosis=new Diagnosis(report,date);

        test.setDiagnosis(diagnosis);

        if(test.getDiagnosis()!=null)
            return true;
        else
            return false;

    }

}
