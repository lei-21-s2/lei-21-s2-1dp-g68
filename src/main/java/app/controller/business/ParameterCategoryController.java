package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.ParameterCategory;

import java.util.List;

public class ParameterCategoryController {

	private final Company company;

	public ParameterCategoryController ( ) {
		this.company = App.getInstance( ).getCompany();
	}

	public boolean createParameterCategory ( ParameterCategory parameterCategory ) {
		return this.company.addParameterCategory( parameterCategory );
	}

	public List< ParameterCategory > getParameterCategories () {
		return this.company.getParameterCategoryList();
	}
}
