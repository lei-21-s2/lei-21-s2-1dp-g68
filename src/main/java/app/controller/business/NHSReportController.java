package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Test;

import java.util.Date;
import java.util.List;

public class NHSReportController {

    private Company company;

    public NHSReportController() {
        this.company = App.getInstance().getCompany();
    }

    private List<Test> getTestbydate(Date begindate, Date enddate){
        return this.company.getTestbetweenDates(begindate,enddate);
    }


}
