package app.controller.business;

import app.controller.App;
import app.domain.model.ClinicalAnalysisLaboratory;
import app.domain.model.Company;

public class CreateClinicalAnalysisLaboratoryController {

	private final Company company;

	public CreateClinicalAnalysisLaboratoryController ( ) {
		this.company = App.getInstance( ).getCompany( );
	}

	public boolean createClinicalAnalysisLaboratory ( ClinicalAnalysisLaboratory clinicalAnalysisLaboratory ) {
		return this.company.addClinicalAnalysisLaboratory( clinicalAnalysisLaboratory );
	}
}
