package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Diagnosis;
import app.domain.model.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class ValidateTestController {

    private Company company;
    private Date date;

    public ValidateTestController() {
        this.company= App.getInstance().getCompany();
    }

    public boolean validateTest(int position){
        List<Test> testList=company.getTestList();

        for (int i = 0; i < testList.size( ); i++) {
          if(i == position){
              date = new Date();
              testList.get(i).setValidationDate(date);
              try {
                  FileWriter myWriter = new FileWriter("emailAndSMSMessages.txt", true);
                  myWriter.write("Email: " + testList.get(i).getClient().getEmail() + "\nMessage: Dear client, your results are already available in the central application.\n\n");
                  myWriter.close();

              } catch (IOException e) {
                  e.printStackTrace();
                  return false;
              }
          }
        }
        return true;
    }

}
