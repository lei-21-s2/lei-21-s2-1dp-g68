package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.TestType;

import java.util.List;

public class CreateTestTypeController {

    private final Company company;

    public CreateTestTypeController ( ) {
        this.company = App.getInstance( ).getCompany( );
    }

    public boolean createTypeTest ( TestType typeTest ) {
        if(typeTest.getModule().equals("1")){
            typeTest.setModule("app.adapter.ExternalModule2");
        } else if (typeTest.getModule().equals("2")){
            typeTest.setModule("app.adapter.ExternalModule1");
        }
        return this.company.addTypeTest( typeTest );
    }

    public List< TestType > getTestTypeList( ) {
        return this.company.getTestTypeList();
    }
}
