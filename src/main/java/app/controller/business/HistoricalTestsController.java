package app.controller.business;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Company;

import java.util.ArrayList;

public class HistoricalTestsController {

    private Company company;

    public HistoricalTestsController(){
        this.company = App.getInstance().getCompany();
    }

    public ArrayList<Client> listOfClients(){
        return this.company.clientList();
    }

}
