package app.controller.business;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.model.Test;
import auth.UserSession;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

public class ViewTestResultsController {

    private Company company;

    public ViewTestResultsController(){
        this.company = App.getInstance().getCompany();
    }

    public ArrayList<Test> listOfTestsClient(UserSession client){
        return this.company.listTestClient(client);
    }

    public ArrayList<Test> listOfTestsClient(Client client){
        return this.company.listTestClient1(client);
    }
}
