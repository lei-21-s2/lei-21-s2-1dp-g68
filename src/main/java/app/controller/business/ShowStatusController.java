package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ShowStatusController {
    private Company company;
    private Date beginDate;
    private Date endDate;
    private String algoritm;
    public ShowStatusController() {
        this.company= App.getInstance().getCompany();
    }

    public Map<String, List<Integer>> getLIstbyBruteForceAlgorithm(){

        return this.company.getSearchAlgoritmInt().getLIstbyBruteForceAlgorithm(beginDate,endDate,this.company.getTestbetweenDates(beginDate, endDate));
    }


    public List<String> algoritms(){
        List<String> set=new LinkedList<>();
        set.add("brute-force algorithm");
        return set;
    }

    public String getBeginDate() {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-mm-yyyy");
        return simpleDateFormat.format(beginDate);
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-mm-yyyy");
        return simpleDateFormat.format(endDate);
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAlgoritm() {
        return algoritm;
    }

    public void setAlgoritm(String algoritm) {
        this.algoritm = algoritm;
    }
}
