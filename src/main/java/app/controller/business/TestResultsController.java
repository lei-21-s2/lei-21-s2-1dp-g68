package app.controller.business;

import app.adapter.ReferenceValuesAPI;
import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.Test;
import com.example2.EMRefValue;
import net.sourceforge.barbecue.output.OutputException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class TestResultsController {

    private Company company;

    public TestResultsController(){
        this.company= App.getInstance().getCompany();
    }

    public boolean registerTestResults(LocalDate date, String parameterCode, String result, String metrics) throws InstantiationException, IllegalAccessException, ClassNotFoundException, OutputException, IOException {

        Test test=getTestByParameter(parameterCode);

        Parameter parameter=getParameterByParameterCode(parameterCode);

        String module=test.getType().getModule();

        Class<?> c=Class.forName(module);

        ReferenceValuesAPI rv= (ReferenceValuesAPI) c.newInstance();

        EMRefValue reference=rv.getReferenceValues(parameter);

        return company.getTestByCode(test.getInternalCode()).getTestForParameter(parameterCode).addTestResult(date,result,metrics,reference);
    }

    public Test getTestByParameter(String parameterCode){
        List<Test> tests=company.getTestList();

        for(Test t : tests){
            for(Parameter p : t.getpList()){
                if(p.getCode().equals(parameterCode)){
                    return t;
                }
            }
        }
        throw new NullPointerException("No tests with that code parameter");
    }

    public Parameter getParameterByParameterCode(String parameterCode){
        Set<Parameter> parameters=company.getParameterList();

        for(Parameter p : parameters){
            if(p.getCode().equals(parameterCode)){
                return p;
            }
        }
        throw new NullPointerException("No parameters with that code");
    }
}
