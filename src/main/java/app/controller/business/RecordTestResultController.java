package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Test;

public class RecordTestResultController {

	private Company company;

	public RecordTestResultController ( ) {
		this.company = App.getInstance( ).getCompany();
	}


	public Iterable< Test > getTestList () {
		return company.getTestList();
	}

}
