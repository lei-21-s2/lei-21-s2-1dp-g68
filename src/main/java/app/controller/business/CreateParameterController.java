package app.controller.business;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Parameter;

public class CreateParameterController {

	private Company company;

	public CreateParameterController ( ) {
		this.company = App.getInstance( ).getCompany();
	}

	public boolean createParameter ( Parameter parameter ) {
		return this.company.addParameter( parameter );
	}

}
