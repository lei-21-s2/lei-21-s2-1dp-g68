package app.controller.business;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Company;

import java.util.ArrayList;
import java.util.Set;

public class ConsultHistoricalTestsController {

    private Company company;

    public ConsultHistoricalTestsController() {
        this.company = App.getInstance().getCompany();
    }

    public Set<Client> getClientList(){
        return this.company.getClientList();
    }
}
