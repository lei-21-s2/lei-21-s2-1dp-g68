package app.controller.business;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Company;

public class UpdatePersonalDataController {

    private final Company company;

    public UpdatePersonalDataController(){
        this.company = App.getInstance( ).getCompany( );
    }

    public boolean updatePersonalData(Client client){

        int pos=0;

        for(Client c : company.getClientList()){
            if(c.getEmail().equals(client.getEmail())){
                return this.company.editClient(client,pos);
            }
            pos++;
        }

        return false;
    }

}
