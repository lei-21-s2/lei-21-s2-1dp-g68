package app.controller;

import app.domain.model.*;
import app.domain.shared.Constants;
import auth.AuthFacade;
import auth.UserSession;
import com.example2.EMRefValue;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.*;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class App {

	private final Company company;
	private final AuthFacade authFacade;

	private App ( ) {
		Properties props = getProperties( );
		this.company = new Company( props.getProperty( Constants.PARAMS_COMPANY_DESIGNATION ),  props.getProperty("SumAdpter.class"), props.getProperty("Brute-force.class")  );
		this.authFacade = this.company.getAuthFacade( );
		bootstrap( );
	}

	public Company getCompany ( ) {
		return this.company;
	}


	public UserSession getCurrentUserSession ( ) {
		return this.authFacade.getCurrentUserSession( );
	}

	public boolean doLogin ( String email, String pwd ) {
		return this.authFacade.doLogin( email, pwd ).isLoggedIn( );
	}

	public void doLogout ( ) {
		this.authFacade.doLogout( );
	}

	private Properties getProperties ( ) {
		Properties props = new Properties( );

		// Add default properties and values
		props.setProperty( Constants.PARAMS_COMPANY_DESIGNATION, "Many Labs" );

		// Read configured values
		try {
			InputStream in = new FileInputStream( Constants.PARAMS_FILENAME );
			props.load( in );
			in.close( );
		} catch ( IOException ex ) {

		}
		return props;
	}


	private void bootstrap ( ) {
		this.authFacade.addUserRole( Constants.ROLE_ADMIN, Constants.ROLE_ADMIN );
        this.authFacade.addUserRole( Constants.ROLE_DOCTOR, Constants.ROLE_DOCTOR );
        this.authFacade.addUserRole( Constants.ROLE_RECEPTIONIST, Constants.ROLE_RECEPTIONIST );
		this.authFacade.addUserRole( Constants.ROLE_SPECIALIST_DOCTOR, Constants.ROLE_SPECIALIST_DOCTOR );
		this.authFacade.addUserRole( Constants.ROLE_LABORATORY_COORDINATOR, Constants.ROLE_LABORATORY_COORDINATOR );
        this.authFacade.addUserRole( Constants.ROLE_CLIENT, Constants.ROLE_CLIENT );
		this.authFacade.addUserRole( Constants.ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST, Constants.ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST);
        this.authFacade.addUserWithRole( "Joana Maria", "joanam@manylabs.pt", "joanajoana", Constants.ROLE_RECEPTIONIST );
		this.authFacade.addUserWithRole( "Rui Pedro", "ruip@manylabs.pt", "rui123", Constants.ROLE_CLIENT );
		this.authFacade.addUserWithRole( "Rute Carla", "rute@manylabs.pt", "carla123", Constants.ROLE_DOCTOR );
		this.authFacade.addUserWithRole( "Pedro Morais", "pedro@manylabs.pt", "pedro123", Constants.ROLE_SPECIALIST_DOCTOR );
		this.authFacade.addUserWithRole( "Miguel Carvalho", "miguel@manylabs.pt", "miguel123", Constants.ROLE_LABORATORY_COORDINATOR );
		this.authFacade.addUserWithRole( "Joao Carvalho", "joao@manylabs.pt", "joao123", Constants.ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST );
		this.authFacade.addUserWithRole( "Main Administrator", "a@g.pt", "1", Constants.ROLE_ADMIN );

		Client client=new Client("António Barros", "antoniomsbarros@gmail.com","male", new Date(), 123, 3211L, 125L, 987654321L );
		this.company.addClient(client);

		Client client1 = new Client("Rui Pedro","ruip@manylabs.pt","male",new Date(),1234,1234L,122L,917806336L);
		this.company.addClient(client1);

		TestType tt=new TestType("blood test", "1", "needle","app.adapter.ExternalModule2");
		this.company.addTypeTest(tt);

		ParameterCategory parameterCategory=new ParameterCategory("ESR00","pc", "123" ,tt);
		this.company.addParameterCategory(parameterCategory);
		Set<ParameterCategory> parameterCategories=new HashSet<>();
		parameterCategories.add(parameterCategory);

		Parameter parameter=new Parameter("ESR00","name", "shortDescription",parameterCategory);
		this.company.addParameter(parameter);
		Set<Parameter> parameters=new HashSet<>();
		parameters.add(parameter);

		Test test=new Test("000000000000","123456789abc",tt,parameterCategories,parameters,client);
		Test test4=new Test("000000000004","123456789xbc",tt,parameterCategories,parameters,client);
		Test test5=new Test("000000000005","123456789zbc",tt,parameterCategories,parameters,client);
		company.addTest(test);
		company.addTest(test4);
		company.addTest(test5);
		LocalDate localDate = LocalDate.of(2020,01,01);
		LocalDate localDate1 = LocalDate.of(2020,01,01);
		Date date1 = java.sql.Date.valueOf(localDate1);
		EMRefValue ref = new EMRefValue("123","globulos brancos",2.0,3.0,date1);

		TestResult testResult = new TestResult(localDate,"bom","glóbulos brancos",ref);

		List<Sample> sampleList = new ArrayList<>();
		LocalDate localDate2 = LocalDate.of(2020,01,01);
		Date date2 = java.sql.Date.valueOf(localDate2);

		Sample sample1 = new Sample(date2,"12345678");
		sampleList.add(sample1);

		LocalDate localDate3 = LocalDate.of(2020,01,01);
		Diagnosis diagnosis = new Diagnosis("boa saude",localDate3);

		LocalDate localDate4 = LocalDate.of(2020,01,01);
		Date datecriation = java.sql.Date.valueOf(localDate4);

		LocalDate localDate5 = LocalDate.of(2020,01,01);
		Date datevalidation = java.sql.Date.valueOf(localDate5);

		Test test1 = new Test("000000000001","1a2s3d4f5g6h",tt,parameterCategories,parameters,testResult,client1,sampleList,diagnosis,datecriation,datevalidation);
		this.company.addTest(test1);


		LocalDate localDate10 = LocalDate.of(2020,01,01);
		Date datecriation2 = java.sql.Date.valueOf(localDate10);
		LocalDate localDate6 = LocalDate.of(2020,01,11);
		Date datevalidation2 = java.sql.Date.valueOf(localDate6);
		Test test2 =new Test("000000000002","1a2s3d4f5g6h",tt,parameterCategories,parameters,testResult,client1,sampleList,diagnosis,datecriation2,datevalidation2);
		this.company.addTest(test2);

		LocalDate localDate11 = LocalDate.of(2020,01,10);
		Date datecriation11 = java.sql.Date.valueOf(localDate11);
		LocalDate localDate7 = LocalDate.of(2020,01,15);
		Date datevalidation3 = java.sql.Date.valueOf(localDate7);
		Test test3 =new Test("000000000003","1a2s3d4f5g6h",tt,parameterCategories,parameters,testResult,client1,sampleList,diagnosis,datecriation11,datevalidation3);
		this.company.addTest(test3);
	}

	// Extracted from https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
	private static App singleton = null;

	public static App getInstance ( ) {
		if ( singleton == null ) {
			synchronized ( App.class ) {
				singleton = new App( );
			}
		}
		return singleton;
	}

}
