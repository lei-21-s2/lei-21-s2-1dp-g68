package app.ui.console.roles;

import app.controller.register.RegisterLaboratoryCoordinatorController;
import app.ui.console.MenuItem;
import app.ui.console.business.ValidateTestUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class LaboratoryCoordinatorUI implements Runnable {

    private final RegisterLaboratoryCoordinatorController registerLaboratoryCoordinatorController;

    public LaboratoryCoordinatorUI(){
        this.registerLaboratoryCoordinatorController=new RegisterLaboratoryCoordinatorController();
    }

    public LaboratoryCoordinatorUI(RegisterLaboratoryCoordinatorController registerLaboratoryCoordinatorController){
        this.registerLaboratoryCoordinatorController=registerLaboratoryCoordinatorController;
    }

    public void run(){

        List<MenuItem> options=new ArrayList<>();
        options.add( new MenuItem( "Validate the work done by the clinical chemistry technologist and specialist doctor",new ValidateTestUI()));

        int option = 0;
        do{
            option= Utils.showAndSelectIndex(options, "\n\nLaboratory Coordinator Menu:");

            if((option>=0)&&(option<options.size())){
                options.get(option).run();
            }
        }
        while (option!=-1);
    }

}
