package app.ui.console.roles;

import app.Main;
import app.ui.console.business.RegisterSampleUI;
import app.ui.console.business.RegisterTestUI;
import app.ui.console.utils.Utils;
import app.controller.roles.ReceptionistController;
import app.ui.console.MenuItem;
import app.ui.console.client.RegisterClientUI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class ReceptionistUI implements Runnable {

	private final ReceptionistController receptionistController;

	public ReceptionistUI ( ) {
		this.receptionistController = new ReceptionistController( );
	}

	public ReceptionistUI ( ReceptionistController receptionistController ) {
		this.receptionistController = receptionistController;
	}

	public void run ( ) {

		List< MenuItem > options = new ArrayList<>( );
		options.add( new MenuItem( "Register new client", new RegisterClientUI( ) ) );
		options.add(new MenuItem("Register Test to a Client", new RegisterTestUI()));
		options.add(new MenuItem("Record Sample", new RegisterSampleUI()));
		int option = 0;
		do {
			option = Utils.showAndSelectIndex( options, "\n\nReceptionist Menu:" );

			if ( ( option >= 0 ) && ( option < options.size( ) ) ) {
				options.get( option ).run( );
			}
		}
		while ( option != -1 );
	}

}
