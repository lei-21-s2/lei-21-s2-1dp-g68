package app.ui.console.roles;

import app.controller.register.RegisterSpecialistDoctorController;
import app.ui.console.MenuItem;
import app.ui.console.business.MakeReportUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SpecialistDoctorUI implements Runnable {

    private final RegisterSpecialistDoctorController registerSpecialistDoctorController;

    public SpecialistDoctorUI(){
        this.registerSpecialistDoctorController=new RegisterSpecialistDoctorController();
    }

    public SpecialistDoctorUI(RegisterSpecialistDoctorController registerSpecialistDoctorController){
        this.registerSpecialistDoctorController=registerSpecialistDoctorController;
    }

    public void run(){

        List<MenuItem> options=new ArrayList<>();
        options.add( new MenuItem( "Make the diagnosis and write a report for a given test",new MakeReportUI()));

        int option = 0;
        do{
            option=Utils.showAndSelectIndex(options, "\n\nSpecialist Doctor Menu:");

            if((option>=0)&&(option<options.size())){
                options.get(option).run();
            }
        }
        while (option!=-1);
    }

}
