package app.ui.console.roles;

import app.controller.roles.ClinicalChemistryTechnologistController;
import app.ui.console.MenuItem;
import app.ui.console.business.RecordTestResultsUI;
import app.ui.console.business.RegisterSampleUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ClinicalChemistryTechnologistUI implements Runnable {

	private final ClinicalChemistryTechnologistController clinicalChemistryTechnologistController;

	public ClinicalChemistryTechnologistUI ( ) {
		this.clinicalChemistryTechnologistController = new ClinicalChemistryTechnologistController();
	}

	public void run ( ) {

		List< MenuItem > options = new ArrayList<>( );
		options.add( new MenuItem( "Record Test Results", new RecordTestResultsUI( ) ) );
		options.add(new MenuItem("Record Sample", new RegisterSampleUI()));
		int option = 0;
		do {
			option = Utils.showAndSelectIndex( options, "\n\nClinical Chemistry Technologist Menu:" );

			if ( ( option >= 0 ) && ( option < options.size( ) ) ) {
				options.get( option ).run( );
			}
		}
		while ( option != -1 );
	}

}