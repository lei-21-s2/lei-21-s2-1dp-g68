package app.ui.console.roles;

import app.controller.roles.ClientController;
import app.ui.console.MenuItem;
import app.ui.console.client.UpdatePersonalDataUI;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ClientUI implements Runnable{

    private final ClientController clientController;

    public ClientUI(){
        this.clientController=new ClientController();
    }

    public ClientUI(ClientController clientController){
        this.clientController=clientController;
    }

    public void run(){

        List<MenuItem> options=new ArrayList<>();
        options.add(new MenuItem("Update Personal Data",new UpdatePersonalDataUI()));
        int option=0;
        do{
            option=Utils.showAndSelectIndex(options,"\n\nClient Menu:");

            if((option>=0) && (option<options.size())){
                options.get(option).run();
            }
        }
        while(option!=-1);
    }

}
