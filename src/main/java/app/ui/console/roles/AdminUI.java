package app.ui.console.roles;

import app.ui.console.business.*;
import app.ui.console.client.RegisterClientUI;
import app.ui.console.employees.RegisterEmployeeUI;
import app.ui.console.utils.Utils;
import app.ui.console.MenuItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */

public class AdminUI implements Runnable {

	public AdminUI ( ) {
	}

	public void run ( ) {
		List< MenuItem > options = new ArrayList<>( );
		options.add( new MenuItem( "Register a new Client", new RegisterClientUI( ) ) );
		options.add( new MenuItem( "Register a new Employee ", new RegisterEmployeeUI( ) ) );
		options.add(new MenuItem("Register a new clinical analysis laboratory",new ClinicalAnalysisLaboratoryUI()));
		options.add( new MenuItem( "Create a Test Type", new CreateTestTypeUI() ) );
		options.add( new MenuItem( "Create a Parameter", new CreateParameterUI() ) );
		options.add( new MenuItem( "Create a Parameter Category", new CreateParameterCategoryUI() ) );
		options.add(new MenuItem("Record Sample", new RegisterSampleUI()));
		options.add( new MenuItem( "Record Test Results", new RecordTestResultsUI( ) ) );
		options.add(new MenuItem("Make report",new MakeReportUI()));
		int option = 0;
		do {
			option = Utils.showAndSelectIndex( options, "\n\nAdmin Menu:" );

			if ( ( option >= 0 ) && ( option < options.size( ) ) ) {
				options.get( option ).run( );
			}
		}
		while ( option != -1 );
	}

}
