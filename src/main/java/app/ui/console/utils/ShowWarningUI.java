package app.ui.console.utils;

public class ShowWarningUI implements Runnable {
	
	private String warningMessage;
	private int padding;
	private int margin;

	public ShowWarningUI ( String warningMessage ) {
		this.warningMessage = warningMessage;
		this.margin = 2;
		this.padding = 8;
	}

	@Override
	public void run ( ) {

		System.out.println( );
		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) + 2; i++ ) {
			System.out.print( "!" );
		}


		System.out.print( "\n!" );
		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) ; i++ ) {
			System.out.print( " " );
		}
		System.out.print( "!" );
		System.out.print( "\n!" );
		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) ; i++ ) {
			System.out.print( " " );
		}
		System.out.print( "!" );

		System.out.print( "\n!");
		for ( int i = 0; i < padding; i++ ) {
			System.out.print( " " );
		}
		System.out.print( warningMessage );
		for ( int i = 0; i < padding; i++ ) {
			System.out.print( " " );
		}
		System.out.print( "!");

		System.out.print( "\n!" );
		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) ; i++ ) {
			System.out.print( " " );
		}
		System.out.print( "!" );

		System.out.print( "\n!" );
		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) ; i++ ) {
			System.out.print( " " );
		}
		System.out.print( "!\n" );

		for ( int i = 0; i < warningMessage.length() + ( padding * 2 ) + 2 ; i++ ) {
			System.out.print( "!" );
		}
	}

}
