package app.ui.console;

import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class MainMenuUI implements Runnable {

	public MainMenuUI ( ) {
	}

	public void run ( ) {
		List< MenuItem > options = new ArrayList< >( );
		int option = 0;

		options.add( new MenuItem( "Login", new AuthUI( ) ) );
		options.add( new MenuItem( "Know the Development Team", new DevTeamUI( ) ) );

		do {
			option = Utils.showAndSelectIndex( options, "\n\nMain Menu" );

			if ( ( option >= 0 ) && ( option < options.size( ) ) ) {
				options.get( option ).run( );
			}
		}
		while ( option != -1 );
	}


}
