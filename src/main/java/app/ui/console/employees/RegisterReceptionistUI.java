package app.ui.console.employees;

import app.controller.register.RegisterReceptionistController;
import app.domain.model.Receptionist;
import app.ui.console.utils.Utils;

public class RegisterReceptionistUI implements Runnable {

	private final RegisterReceptionistController registerReceptionistController;

	public RegisterReceptionistUI ( ) {
		registerReceptionistController = new RegisterReceptionistController( );
	}

	@Override
	public void run ( ) {
		String name = Utils.readLineFromConsole( "Enter the name: " );
		String email = Utils.readLineFromConsole( "Enter the email: " );
		String password = Utils.readLineFromConsole( "Enter the password: " );
		String employeeId = Utils.readLineFromConsole( "Enter the employee ID: " );
		String address = Utils.readLineFromConsole( "Enter the address: " );
		String socCode = Utils.readLineFromConsole( "Enter the socCode: " );
		String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );


		try {
			Receptionist receptionist = new Receptionist( employeeId, name, address, phoneNumber, email, socCode );

			if ( registerReceptionistController.registerReceptionist( receptionist, password ) ) {
				System.out.println( "Successfully registered the receptionist\t" + name );
			} else {
				System.out.println( "There was a problem and the receptionist was not created." );
			}

		} catch( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "There was a problem and the receptionist was not created." );
		}
	}

}
