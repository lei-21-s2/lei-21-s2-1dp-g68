package app.ui.console.employees;

import app.controller.roles.ClinicalChemistryTechnologistController;
import app.domain.model.ClinicalChemistryTechnologist;
import app.ui.console.utils.Utils;

public class RegisterClinicalChemistryTechnologistUI implements Runnable {

	private ClinicalChemistryTechnologistController clinicalChemistryTechnologistController;

	public RegisterClinicalChemistryTechnologistUI ( ) {
		this.clinicalChemistryTechnologistController = new ClinicalChemistryTechnologistController( );
	}

	@Override
	public void run ( ) {

		System.out.println("\nRegister Clinical Chemistry Technologist");

		String name = Utils.readLineFromConsole( "Enter the name: " );
		String email = Utils.readLineFromConsole( "Enter the email: " );
		String password = Utils.readLineFromConsole( "Enter the password: " );
		String employeeId = Utils.readLineFromConsole( "Enter the employee ID: " );
		String address = Utils.readLineFromConsole( "Enter the address: " );
		String socCode = Utils.readLineFromConsole( "Enter the socCode: " );
		String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );

		try {
			ClinicalChemistryTechnologist clinicalChemistryTechnologist =
					new ClinicalChemistryTechnologist( employeeId, name, address, phoneNumber, email, socCode );

			if ( this.clinicalChemistryTechnologistController.registerClinicalChemistryTechnologist( clinicalChemistryTechnologist, password ) ) {
				System.out.println( "\nSuccessfully registered the clinical chemistry technologist\t" + name );
			} else {
				System.out.println( "\nThere was a problem and the clinical chemistry technologist was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "\nThere was a problem and the clinical chemistry technologist was not created." );
		}
	}

}
