package app.ui.console.employees;

import app.controller.register.RegisterDoctorController;
import app.domain.model.Doctor;
import app.ui.console.utils.Utils;

public class RegisterDoctorUI implements Runnable {

	private RegisterDoctorController registerDoctorController;

	public RegisterDoctorUI ( ) {
		registerDoctorController = new RegisterDoctorController( );
	}

	@Override
	public void run ( ) {
		String name = Utils.readLineFromConsole( "Enter the name: " );
		String email = Utils.readLineFromConsole( "Enter the email: " );
		String password = Utils.readLineFromConsole( "Enter the password: " );
		String employeeId = Utils.readLineFromConsole( "Enter the employee ID: " );
		String address = Utils.readLineFromConsole( "Enter the address: " );
		String socCode = Utils.readLineFromConsole( "Enter the socCode: " );
		String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );
		String doctorIndexNumber = Utils.readLineFromConsole( "Enter the doctorIndexNumber: " );

		try {
			Doctor doctor = new Doctor( employeeId, name, address, phoneNumber, email, socCode, doctorIndexNumber );

			if ( this.registerDoctorController.registerDoctor( doctor, password ) ) {
				System.out.println( "\nSuccessfully registered the doctor\t" + name );
			} else {
				System.out.println( "\nThere was a problem and the doctor was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "\nThere was a problem and the doctor was not created." );
		}
	}

}
