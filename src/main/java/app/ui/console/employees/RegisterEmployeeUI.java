package app.ui.console.employees;

import app.ui.console.MenuItem;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class RegisterEmployeeUI implements Runnable {

	public RegisterEmployeeUI ( ) {
	}

	@Override
	public void run ( ) {

		System.out.println("\nRegister a new Employee");

		List< MenuItem > options = new ArrayList<>( );
		options.add( new MenuItem( "Register a new receptionist.", new RegisterReceptionistUI( ) ) );
		options.add( new MenuItem( "Register a new doctor.", new RegisterDoctorUI( ) ) );
		options.add( new MenuItem( "Register a new coordinator.", new RegisterLaboratoryCoordinatorUI( ) ) );
		options.add( new MenuItem( "Register a new specialist doctor.", new RegisterSpecialistDoctorUI()));
		//options.add( new MenuItem( "Register new tech.", new RegisterTechUI( ) ) );

		int option = 0;
		do {
			option = Utils.showAndSelectIndex( options, "\n\nWhich employee are you registering?" );

			if ( ( option >= 0 ) && ( option < options.size( ) ) ) {
				options.get( option ).run( );
			}
		}
		while ( option != -1 );
	}

}
