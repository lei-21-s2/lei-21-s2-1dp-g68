package app.ui.console.employees;

import app.controller.register.RegisterLaboratoryCoordinatorController;
import app.domain.model.LaboratoryCoordinator;
import app.ui.console.utils.Utils;

public class RegisterLaboratoryCoordinatorUI implements Runnable {

    private RegisterLaboratoryCoordinatorController registerLaboratoryCoordinatorController;

    public RegisterLaboratoryCoordinatorUI() {
        registerLaboratoryCoordinatorController=new RegisterLaboratoryCoordinatorController();
    }

    @Override
    public void run ( ) {
        String name = Utils.readLineFromConsole( "Enter the name: " );
        String email = Utils.readLineFromConsole( "Enter the email: " );
        String password = Utils.readLineFromConsole( "Enter the password: " );
        String employeeId = Utils.readLineFromConsole( "Enter the employee ID: " );
        String address = Utils.readLineFromConsole( "Enter the address: " );
        String socCode = Utils.readLineFromConsole( "Enter the socCode: " );
        String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );
        String coordinatorIndexNumber = Utils.readLineFromConsole( "Enter the coordinatorIndexNumber: " );

        try {
            LaboratoryCoordinator coordinator = new LaboratoryCoordinator(employeeId,name,address,phoneNumber,email,socCode,coordinatorIndexNumber);

            if(this.registerLaboratoryCoordinatorController.registerLaboratoryCoordinator(coordinator,password)){
                System.out.println( "\nSuccessfully registered the laboratory coordinator\t"+name);
            }else{
                System.out.println( "\nThere was a problem and the laboratory coordinator was not created.");
            }

        }catch(NullPointerException | IllegalArgumentException ignored){
            System.out.println("\nThere was a problem and the laboratory coordinator was not created.");
        }
    }

}
