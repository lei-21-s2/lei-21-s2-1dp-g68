package app.ui.console.employees;

import app.controller.register.RegisterSpecialistDoctorController;
import app.domain.model.SpecialistDoctor;
import app.ui.console.utils.Utils;

public class RegisterSpecialistDoctorUI implements Runnable{

    private RegisterSpecialistDoctorController registerSpecialistDoctorController;

    public RegisterSpecialistDoctorUI() {
        registerSpecialistDoctorController=new RegisterSpecialistDoctorController();
    }

    @Override
    public void run ( ) {
        String name = Utils.readLineFromConsole( "Enter the name: " );
        String email = Utils.readLineFromConsole( "Enter the email: " );
        String password = Utils.readLineFromConsole( "Enter the password: " );
        String employeeId = Utils.readLineFromConsole( "Enter the employee ID: " );
        String address = Utils.readLineFromConsole( "Enter the address: " );
        String socCode = Utils.readLineFromConsole( "Enter the socCode: " );
        String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );
        String doctorIndexNumber = Utils.readLineFromConsole( "Enter the specialistdoctorIndexNumber: " );

        try {
            SpecialistDoctor doctor=new SpecialistDoctor(employeeId,name,address,phoneNumber,email,socCode,doctorIndexNumber);

            if(this.registerSpecialistDoctorController.registerSpecialistDoctor(doctor,password)){
                System.out.println( "\nSuccessfully registered the specialist doctor\t"+name);
            }else{
                System.out.println( "\nThere was a problem and the specialist doctor was not created.");
            }

        }catch(NullPointerException | IllegalArgumentException ignored){
            System.out.println("\nThere was a problem and the specialist doctor was not created.");
        }
    }

}
