package app.ui.console.client;

import app.controller.register.RegisterClientController;
import app.domain.model.Client;
import app.ui.console.utils.Utils;

import java.util.Date;

public class RegisterClientUI implements Runnable {

	private final RegisterClientController registerClientController;

	public RegisterClientUI ( ) {
		this.registerClientController = new RegisterClientController( );
	}

	@Override
	public void run ( ) {

		System.out.println("Register Client:\n");

		String name = Utils.readLineFromConsole( "Enter your name: " );
		String email = Utils.readLineFromConsole( "Enter your email address: " );
		String password = Utils.readLineFromConsole( "Enter your password: " );
		String sex = Utils.readLineFromConsole( "Enter your sex: " );
		Date birthDate = Utils.readDateFromConsole( "Enter your birth date (dd-mm-yyyy):" );
		int phoneNumber = Utils.readIntegerFromConsole( "Enter your phone number: " );
		int citizenCardNumber = Utils.readIntegerFromConsole( "Enter your citizen card number: " );
		int NHSnumber = Utils.readIntegerFromConsole( "Enter your NHS number: " );
		int TIFnumber = Utils.readIntegerFromConsole( "Enter your TIF number: " );

		try {
			Client client = new Client( name, email, sex, birthDate, citizenCardNumber, NHSnumber, TIFnumber, phoneNumber );

			if ( this.registerClientController.registerClient( client, password ) ) {
				System.out.println( "Successfully registered the client\t" + name );
			} else {
				System.out.println( "There was a problem and the client was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "There was a problem and the client was not created." );
		}


	}

}
