package app.ui.console;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class DevTeamUI implements Runnable {

    public DevTeamUI()
    {

    }
    public void run()
    {
        System.out.println("\n");
        System.out.printf("Development Team:\n");
        System.out.printf("\t José Lapa - 1200619@isep.ipp.pt \n");
        System.out.printf("\t António Barros - 1200606@isep.ipp.pt \n");
        System.out.printf("\t Beatriz Meireles - 1200607@isep.ipp.pt \n");
        System.out.printf("\t Daniel Reis - 1200608@isep.ipp.pt \n");
        System.out.printf("\t Artur Muiria - 1161274@isep.ipp.pt \n");
        System.out.println("\n");
    }
}
