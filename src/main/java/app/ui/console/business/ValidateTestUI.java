package app.ui.console.business;

import app.controller.App;
import app.controller.business.ValidateTestController;
import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.Test;
import app.ui.console.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ValidateTestUI implements Runnable{

    private ValidateTestController validateTestController;
    private Company company;
    private Date date;
    private SimpleDateFormat formatter;

    public ValidateTestUI() {
        this.validateTestController=new ValidateTestController();
        this.company= App.getInstance().getCompany();
    }

    @Override
    public void run() {
        List<Test> tests = company.getTestList();
        int num = 0;

        if (tests.isEmpty()) {
            System.out.println("There are no tests");
            return;
        }

        try {
            for (Test test : tests) {
                for (Parameter par : test.getpList()) {
                    if ((par.getTestResult().getTrDate() == null && test.getDiagnosis().getDate() == null) || test.getValidationDate() != null) {
                        num++;
                    }
                }
            }

            if (tests.size() == num) {
                System.out.println("There are no tests to validate.");
                return;
            }

        }  catch(NullPointerException e) {
            System.out.println("There are no tests to validate.");
            return;
        }

        System.out.println("List of tests:");
            for (int i = 0; i < tests.size(); i++) {
                for (Parameter par : tests.get(i).getpList()) {
                    if (tests.get(i).getValidationDate() == null && par.getTestResult().getTrDate() != null && tests.get(i).getDiagnosis().getDate() != null) {
                        System.out.println("Number of the test: " + (i + 1) + " - " + "Test registration date: " + tests.get(i).getDateofcriaction() + " - Chemical analysis date: " + par.getTestResult().getTrDate() + " - Diagnosis date: " + tests.get(i).getDiagnosis().getDate() + "\n");
                    }
                }
            }

        int position = Utils.readIntegerFromConsole("Which test would you like to validate: ");

        if (validateTestController.validateTest((position - 1))) {
            formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
            date = new Date(System.currentTimeMillis());
            System.out.println("Test validated at: "+ formatter.format(date) + "\nSuccessfully validated the test and notified the client.");
        } else {
                System.out.println("\nThere was a problem validating the test.");
        }
    }
}
