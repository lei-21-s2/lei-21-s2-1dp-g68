package app.ui.console.business;

import app.controller.App;
import app.controller.business.RegisterTestController;
import app.domain.model.*;
import app.ui.console.utils.Utils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class RegisterTestUI  implements  Runnable{

    private final RegisterTestController rtc;
    private Company company;

    public RegisterTestUI() {
        this.rtc = new RegisterTestController();
        this.company = App.getInstance( ).getCompany( );
    }

    @Override
    public void run() {

        String internalCode;

        List<Test> testList = company.getTestList();
        if(testList.isEmpty()){
            internalCode = "000000000000";
        }else{
            Test lastTest = testList.get(testList.size() -1);
            char[] res = lastTest.getInternalCode().toCharArray();
            boolean temp = true;
            int a = 11;
            while(temp || a<0){
                int b = Character.getNumericValue(res[a]);
                if(b==9){
                    res[a] = '0';
                }else{
                    b++;
                    res[a] = (char) (b+'0');
                    temp=false;
                }
                a--;
            }
            internalCode = String.valueOf(res);
        }

        String nhsCode = Utils.readLineFromConsole("Enter the NHS Code (Must be alphanumeric and a size of 12):");
        Client registerClient = new Client();
        TestType type = new TestType();
        Set<ParameterCategory> pcList = new HashSet<>();
        Set<Parameter> pList = new HashSet<>();

        Set<Client> listClient = company.getClientList();

        if(listClient.isEmpty()){
            System.out.println("There are no client's \n");
            return;
        }

        for(Client ct : listClient){
            System.out.println(ct);
        }

        long tinCode = Long.parseLong(Objects.requireNonNull(Utils.readLineFromConsole("Enter the Tin code of the Client"))) ;
        for(Client ct1 : listClient){
            if(ct1.getTIFnumber() == tinCode){
                registerClient = ct1;
                break;
            }
        }
        if(registerClient == null){
            System.out.println("You didnt select a Client");
            return;
        }

        Set<TestType> listTestType = company.getTypeTestList();

        if(listTestType.isEmpty()){
            System.out.println("There are no Test Type \n");
            return;
        }

        listTestType = company.getTypeTestList();

        for(TestType tt : listTestType){
            System.out.println(tt);
            String res = Utils.readLineFromConsole("Is this the Test Type you want? (Y/N):");
            if(res.isEmpty()){
                System.out.println("You didnt respond");
                while(res.isEmpty()){
                    res = Utils.readLineFromConsole("Is this the Test Type you want? (Y/N):");
                }
            }
            if(res.equals("Y") || res.equals("y")){
                type = tt;
                break;
            }
        }

        if(type == null){
            System.out.println("You didnt select a Test Type");
            return;
        }

        List<ParameterCategory> listCategory = company.getParameterCategoryList();

        if(listCategory.isEmpty()){
            System.out.println("There are no Parameter Category \n");
            return;
        }

        listCategory = company.getParameterCategoryList();

        for(ParameterCategory pc : listCategory) {
            if(pc.getTestType().equals(type)){
                System.out.println(pc);
                String res3 = Utils.readLineFromConsole("Is this the Parameter Category you want? (Y/N):");
                if (res3 == null) {
                    System.out.println("You didnt respond");
                    while(res3 == null){
                        res3 = Utils.readLineFromConsole("Is this the Parameter Category you want? (Y/N):");
                    }
                }
                if (res3.equals("Y") || res3.equals("y")) {
                    pcList.add(pc);
                }
            }
        }

        if(pcList.isEmpty()){
            System.out.println("You didnt select any Parameter Category");
            return;
        }

        Set<Parameter> pl = company.getParameterList();
        if(pl.isEmpty()){
            System.out.println("There are no Parameter \n");
            return;
        }

        pl = company.getParameterList();

        for(Parameter p : pl){
            for (ParameterCategory paraC : pcList){
                if(p.getCategory().equals(paraC)){
                    System.out.println(p);
                    String res4 = Utils.readLineFromConsole("Do you want this Parameter? (Y/N):");
                    if(res4 == null){
                        System.out.println("You didnt respond");
                        while(res4 == null){
                            res4 = Utils.readLineFromConsole("Is this the Parameter Category you want? (Y/N):");
                        }
                    }
                    if(res4.equals("Y") || res4.equals("y")){
                        pList.add(p);
                    }
                }
            }
        }

        if(pl.isEmpty()){
            System.out.println("You didnt select any Parameter");
            System.out.println("Select a Parameter");
            return;
        }

        try {
            Test test = new Test(internalCode,nhsCode,type,pcList,pList,registerClient);
            if ( this.rtc.registerTest(test)) {
                System.out.println( "\nSuccessfully created a  test" );
            } else {
                System.out.println( "\nThere was a problem and the  test was not created." );
            }

        } catch ( NullPointerException | IllegalArgumentException ignored ) {
            System.out.println( "\nThere was a problem and the  test was not created." );
        }


    }
}
