package app.ui.console.business;

import app.controller.App;
import app.controller.business.CreateSampleController;
import app.domain.model.Company;
import app.domain.model.Test;
import app.ui.console.utils.Utils;

import java.util.List;

public class RegisterSampleUI implements Runnable{

    private CreateSampleController sampleController;
    private Company company;

    public RegisterSampleUI() {
        this.sampleController = new CreateSampleController();
        this.company =  App.getInstance( ).getCompany( );
    }

    @Override
    public void run() {

        System.out.println("\nRecord Sample");

        List<Test> fd = company.getTestList();
        int number=-1;
        System.out.println("List of tests:");
        System.out.println(fd.size());
        if (fd.isEmpty()){
            System.out.println("please insert tests!!");
            return;
        }
        int i;
        do {
            for (i = 0; i < fd.size(); i++) {

                System.out.println("N:"+(i+1)+" Number of the test: " +fd.get(i).getInternalCode()+" test Type: "+fd.get(i).getType().getName()
                        +" Client email:"+fd.get(i).getClient().getEmail());

            }
            String read="";
                   read = Utils.readLineFromConsole("Please insert the number to create Sample:");
            number=Integer.parseInt(read);
            if (number>i){
                System.out.println("Please insert a number!!!");
            }
        }while (number<i);

        Test test= fd.get(number-1);

        String confirm="";
        confirm= Utils.readLineFromConsole("Please Confirm: (y\\n)");
        if (confirm.equals("y")){
            String path= sampleController.createSample(test.getInternalCode(), test.getClient().getEmail());
            System.out.println("Path to JPEG file it the barcode: " +path);
        }

    }
}
