package app.ui.console.business;

import app.controller.App;
import app.controller.business.MakeReportController;
import app.domain.model.Client;
import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.Test;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MakeReportUI implements Runnable{

    private MakeReportController makeReportController;
    private Company company;

    public MakeReportUI() {
        this.makeReportController=new MakeReportController();
        this.company=App.getInstance().getCompany();
    }

    @Override
    public void run(){

        System.out.println("\nMake Report");

        List<Test> tests=company.getTestList();

        if(tests.isEmpty()){
            System.out.println("There are no tests");
            return;
        }

        ArrayList<String> cods=new ArrayList<>();

        System.out.println("List of tests:");

        boolean aux=true;

        for(Test t : tests){
            if(t.getDiagnosis().getReport()==null /*&& t com resultados associados*/){
                for(Parameter p : t.getpList()){
                    if(p.getTestResult()!=null){
                        aux=false;
                    }
                }
                if(aux=true){
                    System.out.println(t);
                    cods.add(t.getInternalCode());
                }
            }
        }

        String code=Utils.readLineFromConsole("Please insert the internal code: ");

        while(!cods.contains(code))
            code=Utils.readLineFromConsole("Please insert a valid internal code: ");

        Test test=new Test();
        Client client=new Client();

        for(Test t : tests){
            if(t.getInternalCode().equals(code)) {
                test=t;
                client=t.getClient();
            }
        }

        //print resultados do teste
        for(Parameter p : test.getpList()){
            if(p.getTestResult()==null){
                System.out.println("There are no results\n");
                return;
            }
            System.out.println(p.toString()+"\n");
            System.out.println("Results: \n"+p.getTestResult().toString());
            System.out.println("\nReference Values : \n"+p.getTestResult().getValues().toString());
        }

        String report=Utils.readLineFromConsole("Please write the report: ");

        if(makeReportController.makeReport(client.getEmail(),report))
            System.out.println("\nSuccess!");
        else
            System.out.println("\nThere was a problem and the diagnosis was not created.");

    }

}
