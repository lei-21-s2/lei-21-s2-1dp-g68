package app.ui.console.business;

import app.controller.business.CreateTestTypeController;
import app.domain.model.TestType;
import app.ui.console.utils.Utils;

public class CreateTestTypeUI implements Runnable {

	private final CreateTestTypeController createTestTypeController;

	public CreateTestTypeUI ( ) {
		this.createTestTypeController = new CreateTestTypeController( );
	}

	@Override
	public void run ( ) {

		System.out.println("\nCreate Test Type");

		String name = Utils.readLineFromConsole( "Enter the name: " );
		String code = Utils.readLineFromConsole( "Enter the code: " );
		String collectionMethod = Utils.readLineFromConsole( "Enter the collection method: " );
		String module = Utils.readLineFromConsole( "Enter the module(1-Blood or 2-Covid): " );

		try {
			TestType typeTest = new TestType( name, code, collectionMethod, module );

			if ( this.createTestTypeController.createTypeTest( typeTest ) ) {
				System.out.println( "\nSuccessfully created a type of test" );
			} else {
				System.out.println( "\nThere was a problem and the type of test was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "\nThere was a problem and the type of test was not created." );
		}
	}

}
