package app.ui.console.business;

import app.controller.App;
import app.controller.business.ParameterCategoryController;
import app.domain.model.Company;
import app.domain.model.ParameterCategory;
import app.domain.model.TestType;
import app.ui.console.utils.Utils;

import java.util.Set;

public class CreateParameterCategoryUI implements Runnable{

	private final ParameterCategoryController parameterCategoryController;
	private Company company;

	public CreateParameterCategoryUI ( ) {
		this.parameterCategoryController = new ParameterCategoryController( );
		this.company = App.getInstance( ).getCompany( );
	}

	@Override
	public void run ( ) {

		System.out.println("\nCreate Parameter Category");

		String code = Utils.readLineFromConsole( "Enter the code (Code must have 4 to 8 chars): " );
		String description = Utils.readLineFromConsole( "Enter the description: " );
		String nhsID = Utils.readLineFromConsole( "Enter the NHS ID: " );

		TestType testType = null;
		Set<TestType> listTestType = company.getTypeTestList();

		for(TestType tt : listTestType){
			System.out.println(tt);
			String res = Utils.readLineFromConsole("Is this the Test Type you want? (Y/N):");
			if(res.isEmpty()){
				System.out.println("You didnt respond");
				while(res.isEmpty()){
					res = Utils.readLineFromConsole("Is this the Test Type you want? (Y/N):");
				}
			}
			if(res.equals("Y") || res.equals("y")){
				testType = tt;
				break;
			}
		}

		try {
			ParameterCategory parameterCategory =  new ParameterCategory( code, description, nhsID, testType);

			if ( this.parameterCategoryController.createParameterCategory( parameterCategory ) ) {
				System.out.println( "\nSuccessfully created a  parameter category." );
			} else {
				System.out.println( "\nThere was a problem and the parameter category was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "\nThere was a problem and the parameter category was not created." );
		}

	}

}
