package app.ui.console.business;

import app.controller.App;
import app.controller.business.CreateClinicalAnalysisLaboratoryController;
import app.domain.model.ClinicalAnalysisLaboratory;
import app.domain.model.Company;
import app.domain.model.TestType;
import app.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ClinicalAnalysisLaboratoryUI implements Runnable {

	private final CreateClinicalAnalysisLaboratoryController createClinicalAnalysisLaboratoryController;
	private Company company;

	public ClinicalAnalysisLaboratoryUI ( ) {
		this.createClinicalAnalysisLaboratoryController=new CreateClinicalAnalysisLaboratoryController();
		this.company=App.getInstance().getCompany();
	}

	@Override
	public void run ( ) {
		int laboratoryId = Utils.readIntegerFromConsole( "Enter the ID: " );
		String name = Utils.readLineFromConsole( "Enter the name: " );
		String address = Utils.readLineFromConsole( "Enter the address: " );
		String phoneNumber = Utils.readLineFromConsole( "Enter the phone number: " );
		int tinNumber = Utils.readIntegerFromConsole( "Enter the tin number: " );

		System.out.println("\nTest types:");

		Set<TestType> listTestType = company.getTypeTestList();

		if(listTestType.isEmpty()){
			System.out.println("There are no Test Type \n");
			return;
		}

		for(TestType tt : listTestType){
			System.out.println(tt.toString());
		}

		int option=1;
		List<TestType> testTypes=new ArrayList<>();

		while(option==1){
			String testtype=Utils.readLineFromConsole("Select the code of the test type:");

			while(company.getTestTypeByCode(testtype)==null)
				testtype=Utils.readLineFromConsole("Select the code of the test type:");

			TestType tt=company.getTestTypeByCode(testtype);

			testTypes.add(tt);

			option=Utils.readIntegerFromConsole("Do you want more test types?\n0-No\n1-Yes");

			while(option!=1 && option!=0)
				option=Utils.readIntegerFromConsole("Do you want more test types?\n0-No\n1-Yes");
		}

		try {
			ClinicalAnalysisLaboratory clinicalAnalysisLaboratory =
					new ClinicalAnalysisLaboratory( laboratoryId, name, address, phoneNumber, tinNumber, testTypes );

			if ( this.createClinicalAnalysisLaboratoryController.createClinicalAnalysisLaboratory( clinicalAnalysisLaboratory ) ) {
				System.out.println( "\nSuccessfully created the laboratory");
			} else {
				System.out.println( "\nThere was a problem and the laboratory was not created." );
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			System.out.println( "\nThere was a problem and the laboratory was not created." );
		}
	}

}
