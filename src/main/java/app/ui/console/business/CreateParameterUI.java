package app.ui.console.business;

import app.controller.business.ParameterCategoryController;
import app.controller.business.CreateParameterController;
import app.domain.model.Parameter;
import app.domain.model.ParameterCategory;
import app.ui.console.utils.ShowErrorUI;
import app.ui.console.utils.ShowWarningUI;
import app.ui.console.utils.Utils;

import java.util.List;

public class CreateParameterUI implements Runnable{

	private final CreateParameterController createParameterController;
	private final ParameterCategoryController parameterCategoryController;

	public CreateParameterUI ( ) {
		this.createParameterController = new CreateParameterController( );
		this.parameterCategoryController = new ParameterCategoryController();
	}

	@Override
	public void run ( ) {

		System.out.println("\nCreate Parameter");

		List< ParameterCategory > parameterCategories = parameterCategoryController.getParameterCategories();

		String code = Utils.readLineFromConsole( "Enter the code (Code length must be 5):  " );
		String name = Utils.readLineFromConsole( "Enter the name (Name length must be smaller than 8): " );
		String shortDescription = Utils.readLineFromConsole( "Enter the short description (Short description length must be smaller than 20 and a string): " );

		if ( parameterCategories.isEmpty() ) {
			String answer = Utils.readLineFromConsole( "There are no parameters yet.\n " +
					"Would you like to create one? (y/n)" );

			if ( answer == null || answer.isEmpty() || !answer.equals( "n" ) ) {
				ShowWarningUI warning = new ShowWarningUI( "As there is no possible category the parameter cannot be created" );
				warning.run();

			}
		} else {
			createParameter( parameterCategories, code, name, shortDescription );
		}
	}

	private void createParameter ( List< ParameterCategory > parameterCategories, String code, String name, String shortDescription ) {
		ParameterCategory category =
				( ParameterCategory ) Utils.showAndSelectOne( parameterCategories, "Parameter Categories: " );

		try {
			Parameter parameter =  new Parameter( code, name, shortDescription, category );

			if ( this.createParameterController.createParameter( parameter ) ) {
				System.out.println( "\nSuccessfully created a test parameter." );
			} else {
				ShowErrorUI error = new ShowErrorUI( "There was a problem and the type of test was not created" );
				error.run();
			}

		} catch ( NullPointerException | IllegalArgumentException ignored ) {
			ShowErrorUI error = new ShowErrorUI( "There was a problem and the type of test was not created" );
			error.run();
		}
	}

}
