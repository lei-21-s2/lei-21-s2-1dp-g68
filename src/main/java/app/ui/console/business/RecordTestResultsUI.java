package app.ui.console.business;

import app.controller.App;
import app.controller.business.TestResultsController;
import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.Test;
import app.ui.console.utils.Utils;
import net.sourceforge.barbecue.output.OutputException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RecordTestResultsUI implements Runnable {

	private Company company;
	private TestResultsController testResultsController;

	public RecordTestResultsUI(){
		this.testResultsController=new TestResultsController();
		this.company=App.getInstance().getCompany();
	}

	@Override
	public void run(){

		System.out.println("\nRecord Test Results");

		try {
			List<Test> tests=company.getTestList();
			List<Test> finalTest=new ArrayList<>();

			for(Test t : tests){
				if(!t.getSampleList().isEmpty()){
					finalTest.add(t);
				}
			}

			if(finalTest.isEmpty()) {
				System.out.println("There are no available tests");
				return;
			}

			System.out.println("List of tests:\n");

			for(Test t : finalTest){
				System.out.println(t.toString());
			}
			String code = Utils.readLineFromConsole("Please insert the internal code of the test:");

			while(company.getTestByCode(code)==null)
				code = Utils.readLineFromConsole("Please insert a valid internal code of the test:");

			Test test=company.getTestByCode(code);

			Set<Parameter> parameters= test.getpList();

			for(Parameter p : parameters) {
				System.out.println(p.toString() + "\n");

				String result = Utils.readLineFromConsole("Please insert the result for that parameter:");
				String metric = Utils.readLineFromConsole("Please insert the metric for that parameter:");

				LocalDate date = LocalDate.now();

				testResultsController.registerTestResults(date, p.getCode(), result, metric);
			}

			System.out.println("Results add successful!");

			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IOException | OutputException e) {
				e.printStackTrace();
			}
		}

	}


