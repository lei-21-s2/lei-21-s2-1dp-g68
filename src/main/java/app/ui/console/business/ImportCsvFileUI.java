package app.ui.console.business;

import app.domain.model.*;

public class ImportCsvFileUI {

    private TestResult testResult;
    private Diagnosis diagnosis;

    public void showClient(Client client){
        System.out.println("Client = { name: " + client.getName() +" email: "+ client.getEmail() +" sex: "+ client.getSex() +" address: "+ client.getAdress() +" birth date: "+ client.getBirthDate() +" citizen card number: "+ client.getCitizenCardNumber() + " nhs number: "+ client.getNHSnumber() +" tif number: "+ client.getTIFnumber() + " phone number: " + client.getPhoneNumber() + " }\n");
    }

    public void showTestType(TestType testType){
        System.out.println("Test Type = { name: " + testType.getName() +" code: "+ testType.getCode() +" collection method: "+ testType.getCollectionMethod() +" module: "+ testType.getModule()  + " }\n");
    }

    public void showParameterCategory(ParameterCategory parameterCategory){
        System.out.println("Parameter Category = { code: " + parameterCategory.getCode() +" description: "+ parameterCategory.getDescription() +" nhs id: "+ parameterCategory.getNhsId() +" test type: "+ parameterCategory.getTestType()  + " }\n");
    }

    public void showTest(Test test, ParameterCategory parameterCategory) {
        System.out.println("Test = { internal code: " + test.getInternalCode() + " nhs code: " + test.getNhsCode() + " test type: " + test.getType() + " parameter category: "+ parameterCategory + " test result: " + test.getTestResult() + " client: " + test.getClient() + " sample list: " + test.getSampleList() + " diagnosis: " + test.getDiagnosis() + " date of creation: " + test.getDateofcriaction() + " date of validation: " + test.getValidationDate() + " }\n");
    }
}
