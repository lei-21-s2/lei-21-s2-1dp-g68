package app.ui.gui;

import app.Main;
import app.controller.AuthController;
import app.domain.shared.Constants;
import app.ui.console.AuthUI;
import app.ui.console.MenuItem;
import app.ui.console.roles.*;
import app.ui.gui.util.Alerts;
import auth.mappers.dto.UserRoleDTO;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class LoginController {

    @FXML
    private Button btLogin;

    @FXML
    private Button btBack;

    @FXML
    private TextField emailField;

    @FXML
    private TextField passwordField;

    private int count=0;

    @FXML
    public void onBtBackAction(){
        Stage stageTheLabelBelongs = (Stage) Main.getPrimaryStage().getScene().getWindow();
        MainMenuController.getLoginStage().close();
        stageTheLabelBelongs.show();
    }

    @FXML
    public void onBtLoginAction(){
        String email=emailField.getText();
        String password=passwordField.getText();

        AuthController ac=new AuthController();

        count++;
        if(count<3)
            run(ac,email,password);
        else{
            Alerts.showAlert("Invalid UserId and/or Password",null,"Too many attempts", Alert.AlertType.ERROR);
            try {
                TimeUnit.SECONDS.sleep(5);
            }catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            Platform.exit();
        }

    }

    private static Stage menuStage;

    public static Stage getMenuStage() {
        return menuStage;
    }

    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Menu");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.menuStage=stage;

            MainMenuController.getLoginStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }

    private boolean doLogin (AuthController ac,String email,String password) {

        int maxAttempts = 3;
        boolean success = false;
        do {
            maxAttempts--;
            String id = email;
            String pwd = password;

            success = ac.doLogin( id, pwd );

            if(!success){
                Alerts.showAlert("Invalid UserId and/or Password",null,"Error, try again later", Alert.AlertType.ERROR);
                //System.out.println( "Invalid UserId and/or Password. \n You have  " + maxAttempts + " more attempt(s)." );
            }
        } while ( !success && count > 3 );

        return success;
    }

    public void run(AuthController ac,String email,String password) {
        boolean success = doLogin(ac,email,password);

        if ( success ) {
            List<UserRoleDTO> roles = ac.getUserRoles( );
            if ( ( roles == null ) || ( roles.isEmpty( ) ) ) {
                Alerts.showAlert("Invalid","User has not any role assigned.",null, Alert.AlertType.INFORMATION);
                System.out.println( "User has not any role assigned." );
            } else {
                UserRoleDTO role = AuthUI.selectsRole( roles );
                if ( !Objects.isNull( role ) ) {
                    List<MenuItem> rolesUI = AuthUI.getMenuItemForRoles();
                    //AuthUI.redirectToRoleUI( rolesUI, role );
                    getMenuUI(role);
                } else {
                    System.out.println( "User did not select a role." );
                }
            }
        }
        AuthUI.logout();
    }

    public void getMenuUI (UserRoleDTO role) {
        if(role.getId().equals(Constants.ROLE_ADMIN)){
            loadPage("/app/ui/gui/AdminMenu.fxml");
        }
        if(role.getId().equals(Constants.ROLE_CLIENT)){
            loadPage("/app/ui/gui/ClientMenu.fxml");
        }

        if(role.getId().equals(Constants.ROLE_CLINICAL_CHEMISTRY_TECHNOLOGIST)) {
            loadPage("/app/ui/gui/ClinicalChemistryTechnologistMenu.fxml");
        }

        if(role.getId().equals(Constants.ROLE_LABORATORY_COORDINATOR)){
            loadPage("/app/ui/gui/LaboratorycoordinatorMenu.fxml");

        }
    }
}
