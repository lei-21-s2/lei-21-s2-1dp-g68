package app.ui.gui;

import app.controller.business.SearchAlgoritm;
import app.ui.gui.util.Alerts;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class ShowStatusFinal implements Initializable {
    private Date begindate;
    private Date enddate;
    private String algoritm;
    private Map<String, List<Integer>> result;
    @FXML
    private TextArea numbers;
    @FXML
    private LineChart<?, ?> graphday;

    @FXML
    private LineChart<?, ?> graphmonth;

    @FXML
    private LineChart<?, ?> graphyear;

    @FXML
    private Button btnreturn;

    public void preparedata(String begindate, String endate, String algoritmo){
        app.controller.business.SearchAlgoritm searchAlgoritm=new SearchAlgoritm();
        try {
            this.begindate=new SimpleDateFormat("dd-MM-yyyy").parse(begindate);
            this.enddate=new SimpleDateFormat("dd-MM-yyyy").parse(endate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.algoritm=algoritm;
        result= searchAlgoritm.getLIstbyBruteForceAlgorithm(this.begindate, this.enddate);

        System.out.println("NUmbers");

        String str="Number of Clients: "+ result.get("Number of Clients:").get(0)+"\n"+
                "Number of Test waiting for the all diagnostic: "+result.get("Number of Test waiting for the all diagnostic:").get(0)+"\n"+
                "Number of Test that missing result: "+result.get("Number of Test that missing result:").get(0);
        numbers.setText(str);
        XYChart.Series series=new XYChart.Series();
        System.out.println("showbyDay"+result.get("Number of Test by days:").size());
        for (int i = 0; i < result.get("Number of Test by days:").size(); i++) {
            System.out.println("Days: "+result.get("Number of Test by days:").get(i));
            Number number=result.get("Number of Test by days:").get(i);
            series.getData().add(new XYChart.Data(String.valueOf(i), number));
        }
        graphday.getData().addAll(series);
        showMonth();
        showYear();
    }
    @FXML
    void btnreturnAction(ActionEvent event) {
        loadPage("/app/ui/gui/LaboratorycoordinatorMenu.fxml");
    }
    private static Stage menuStage;
    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Menu");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.menuStage=stage;

            MainMenuController.getLoginStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }
    public void showbyDay(){
        XYChart.Series series=new XYChart.Series();
        System.out.println("showbyDay"+result.get("Number of Test by days:").size());
        for (int i = 0; i < result.get("Number of Test by days:").size(); i++) {
            System.out.println("Days: "+result.get("Number of Test by days:").get(i));
            series.getData().add(new XYChart.Data(i, result.get("Number of Test by days:").get(i)));
        }
        graphday.getData().addAll(series);
    }
    public void showMonth(){
        XYChart.Series series=new XYChart.Series();
        System.out.println("showMonth"+ result.get("Number of Test by months:").size());
        for (int i = 0; i < result.get("Number of Test by months:").size(); i++) {
            Number number=result.get("Number of Test by months:").get(i);
            series.getData().add(new XYChart.Data(String.valueOf(i),number ));
        }
        graphmonth.getData().addAll(series);
    }
    public void showYear(){
        XYChart.Series series=new XYChart.Series();
        System.out.println("showYear"+result.get("Number of Test by year:").size());
        for (int i = 0; i < result.get("Number of Test by year:").size(); i++) {
            Number number=result.get("Number of Test by year:").get(i);
            series.getData().add(new XYChart.Data(String.valueOf(i),number ));
        }
        graphyear.getData().addAll(series);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
       /* showbyDay();
        Numbers();
        showYear();
        showMonth();*/
    }
}
