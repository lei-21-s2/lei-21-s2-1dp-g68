package app.ui.gui;

import app.Main;
import app.ui.console.business.*;
import app.ui.console.client.RegisterClientUI;
import app.ui.console.employees.RegisterClinicalChemistryTechnologistUI;
import app.ui.console.employees.RegisterEmployeeUI;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;


public class    AdminMenuController {

    @FXML
    private Button btBack;

    @FXML
    private Button btAddClient;

    @FXML
    private Button btAddEmployee;

    @FXML
    private Button btAddCal;

    @FXML
    private Button btCreateTestType;

    @FXML
    private Button btCreateParameter;

    @FXML
    private Button btCreateParameterCategory;

    @FXML
    private Button btRecordSample;

    @FXML
    private Button btRecordTestsResults;

    @FXML
    private Button btMakeReport;

    @FXML
    public void onBtBackAction(){
        Stage stageTheLabelBelongs = (Stage) Main.getPrimaryStage().getScene().getWindow();
        LoginController.getMenuStage().close();
        stageTheLabelBelongs.show();
    }

    @FXML
    public void onBtAddClientAction(){
        RegisterClientUI registerClientUI=new RegisterClientUI();

        registerClientUI.run();
    }

    @FXML
    public void onBtAddEmployeetAction(){
        RegisterEmployeeUI registerEmployeeUI=new RegisterEmployeeUI();

        registerEmployeeUI.run();
    }

    @FXML
    public void onBtAddCalAction(){
        RegisterClinicalChemistryTechnologistUI registerClinicalChemistryTechnologistUI=new RegisterClinicalChemistryTechnologistUI();

        registerClinicalChemistryTechnologistUI.run();
    }

    @FXML
    public void onBtCreateTestTypeAction(){
        CreateTestTypeUI createTestTypeUI=new CreateTestTypeUI();

        createTestTypeUI.run();
    }

    @FXML
    public void onBtCreateParameterAction(){
        CreateParameterUI createParameterUI=new CreateParameterUI();

        createParameterUI.run();
    }

    @FXML
    public void onBtCreateParameterCategoryAction(){
        CreateParameterCategoryUI createParameterCategoryUI=new CreateParameterCategoryUI();

        createParameterCategoryUI.run();
    }

    @FXML
    public void onBtRecordSampleAction(){
        RegisterSampleUI registerSampleUI =new RegisterSampleUI();

        registerSampleUI.run();
    }

    @FXML
    public void onBtRecordTestsResultAction(){
        RecordTestResultsUI recordTestResultsUI =new RecordTestResultsUI();

        recordTestResultsUI.run();
    }

    @FXML
    public void onBtMakeReportAction(){
        MakeReportUI makeReportUI =new MakeReportUI();

        makeReportUI.run();
    }

}
