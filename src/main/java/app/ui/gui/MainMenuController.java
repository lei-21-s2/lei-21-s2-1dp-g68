package app.ui.gui;

import app.Main;
import app.controller.AuthController;
import app.ui.console.DevTeamUI;
import app.ui.gui.util.Alerts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController implements Initializable {

    @FXML
    private Button btLogin;

    @FXML
    private Button btKnowTeam;

    @FXML
    public void onBtLoginAction(){
        loadLoginPage("/app/ui/gui/Login.fxml");
    }

    private static Stage loginStage;

    public static Stage getLoginStage() {
        return loginStage;
    }

    private void loadLoginPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Login Menu");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.loginStage=stage;

            Main.getPrimaryStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }

    @FXML
    public void onBtKnowTeamAction(){
        DevTeamUI dev=new DevTeamUI();
        dev.run();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AuthController ac=new AuthController();
    }
}
