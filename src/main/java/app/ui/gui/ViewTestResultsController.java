package app.ui.gui;

import app.controller.App;
import app.domain.model.Test;
import auth.AuthFacade;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class ViewTestResultsController implements Initializable{
    @FXML
    private ChoiceBox testSelector;

    @FXML
    private TextArea testShower;

    public void list(){

        testSelector.setOnAction((event) -> {
            int selectedIndex = testSelector.getSelectionModel().getSelectedIndex();
            Test selectedItem = (Test) testSelector.getSelectionModel().getSelectedItem();

            testShower.setText(selectedItem.print());
        });

        app.controller.business.ViewTestResultsController ctr = new app.controller.business.ViewTestResultsController();


        ArrayList<Test> lst =ctr.listOfTestsClient(App.getInstance().getCurrentUserSession());
        for(Test ts : lst){
            testSelector.getItems().add(ts);

        }
        System.out.println("ola");
    }

    @FXML
    public void onCBSelect(){
        Test ts = (Test) testSelector.getValue();
        testShower.setText(ts.toString());
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        list();
    }
}
