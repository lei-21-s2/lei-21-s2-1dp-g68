package app.ui.gui;

import app.controller.business.ImportCsvFileController;
import app.ui.gui.util.Alerts;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class ImportCsvFile implements Initializable {

    @FXML
    private Button cancel;
    @FXML
    private Button importing;
    @FXML
    private TextField file;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    public void btnImportCsvAction(ActionEvent actionEvent) throws Exception {
        String filePath = file.getText();
        ImportCsvFileController controller = new ImportCsvFileController();
        if(isPathValid(filePath)) {
            boolean found = false;
            try {
                if (controller.importCsvFile(filePath)) {
                    found = true;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (found) {
                Alerts.showAlert("File status", "Success", null, Alert.AlertType.CONFIRMATION);
            } else {
                Alerts.showAlert("File status", "Invalid file", null, Alert.AlertType.ERROR);
            }
        } else {
            Alerts.showAlert("File status", "Invalid file", null, Alert.AlertType.ERROR);
        }
    }

    public static boolean isPathValid(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException ex) {
            return false;
        }

        return true;
    }

    public void btnCancelAction(ActionEvent actionEvent) {
        loadPage("/app/ui/gui/LaboratorycoordinatorMenu.fxml");
    }

    private static Stage menuStage;
    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Import CSV file");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.menuStage=stage;

            MainMenuController.getLoginStage().close();
            stage.show();
        } catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }

}
