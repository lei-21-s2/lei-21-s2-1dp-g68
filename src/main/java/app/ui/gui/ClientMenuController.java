package app.ui.gui;

import app.Main;
import app.ui.gui.util.Alerts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ClientMenuController {

    @FXML
    private Button btBack;

    @FXML
    private Button btUpdatePersonalData;

    @FXML
    private Button btViewTestResults;

    @FXML
    public void onBtBackAction(){
        Stage stageTheLabelBelongs = (Stage) Main.getPrimaryStage().getScene().getWindow();
        LoginController.getMenuStage().close();
        stageTheLabelBelongs.show();
    }

    @FXML
    public void onBtUpdatePersonalDataAction(){
        /*try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ViewTestResults.fxml"));
            Parent root1 = (Parent) loader.load();
            Stage stg = new Stage();
            stg.setTitle("Test Results");
            stg.setScene(new Scene(root1));
            stg.show();
        }catch (IOException e){
            System.out.println("Cant load");
        }*/
        loadPage("/app/ui/gui/UpdateClientData.fxml");
    }



    @FXML
    public void onBtViewTestsAction(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ViewTestResults.fxml"));
            Parent root1 = (Parent) loader.load();
            Stage stg = new Stage();
            stg.setTitle("Test Results");
            stg.setScene(new Scene(root1));
            stg.show();
        }catch (IOException e){
            System.out.println("Cant load");
        }

        //loadPage("/app/ui/gui/ViewTestResults.fxml");
    }

    private static Stage clientStage;

    public static Stage getClientStage() {
        return clientStage;
    }

    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Update Personal Data");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.clientStage=stage;

            LoginController.getMenuStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }

}
