package app.ui.gui;

import app.ui.gui.util.Alerts;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class Showstatus1 implements Initializable {

    @FXML
    private DatePicker begindate;
    @FXML
    private DatePicker enddate;
    @FXML
    private ComboBox<String> cmbalgorithm;

    @FXML
    private Button cancel;
    @FXML
    private Button Next;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cmbalgorithm.getItems().removeAll();
        cmbalgorithm.getItems().addAll("brute-force algorithm");
    }

    public void btnNextAction(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/app/ui/gui/ShowStatusFinal.fxml"));

            Parent root1 = (Parent) loader.load();
            ShowStatusFinal showStatusFinal=loader.getController();
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
            showStatusFinal.preparedata(simpleDateFormat.format(convertToDateViaInstant(begindate.getValue())),
                    simpleDateFormat.format(convertToDateViaInstant(enddate.getValue())),cmbalgorithm.getSelectionModel().getSelectedItem());

            Stage stg = new Stage();
            stg.setTitle("Overall performance");
            stg.setScene(new Scene(root1));
            stg.show();
        }catch (IOException e){
            System.out.println("Cant load");
        }
    }

    public void btnCancelAction(ActionEvent actionEvent) {
        loadPage("/app/ui/gui/LaboratorycoordinatorMenu.fxml");
    }

    private static Stage menuStage;
    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Menu");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.menuStage=stage;

            MainMenuController.getLoginStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }
    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
