package app.ui.gui;

import app.Main;
import app.ui.console.business.RecordTestResultsUI;
import app.ui.console.business.RegisterSampleUI;
import app.ui.gui.util.Alerts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ClinicalChemistryTechnologistMenuController {

    @FXML
    private Button btRegisterSample;

    @FXML
    private Button btRecordResults;

    @FXML
    private Button btHistoricalTests;

    @FXML
    private Button btBack;

    @FXML
    public void onBtBackAction(){
        Stage stageTheLabelBelongs = (Stage) Main.getPrimaryStage().getScene().getWindow();
        LoginController.getMenuStage().close();
        stageTheLabelBelongs.show();
    }

    @FXML
    public void onBtRegisterSampleAction(){
        RegisterSampleUI registerSampleUI =new RegisterSampleUI();

        registerSampleUI.run();
    }

    @FXML
    public void onBtRecordResultsAction(){
        RecordTestResultsUI recordTestResultsUI =new RecordTestResultsUI();

        recordTestResultsUI.run();
    }

    @FXML
    public void onBtHistoricalTestsAction(){
        loadPage("/app/ui/gui/HistoricalTests.fxml");
    }

    private static Stage calStage;

    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Tests");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.calStage=stage;

            LoginController.getMenuStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }

}
