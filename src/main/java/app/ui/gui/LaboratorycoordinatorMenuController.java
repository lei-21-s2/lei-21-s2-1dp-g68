package app.ui.gui;

import app.Main;
import app.ui.gui.util.Alerts;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class LaboratorycoordinatorMenuController {


    @FXML
    private Button btBack;
    @FXML
    private VBox vbox;

    @FXML
    private Button btShowStatus;

    @FXML
    private Button btImportCsv;

    public void onBtAddClientAction(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/app/ui/gui/Showsatus1.fxml"));
            Parent root1 = (Parent) loader.load();
            Stage stg = new Stage();
            stg.setTitle("Overall performance");
            stg.setScene(new Scene(root1));
            stg.show();
        }catch (IOException e){
            System.out.println("Cant load");
        }

    }

    public void onBtImportCsvAction(ActionEvent actionEvent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/app/ui/gui/ImportCsvFile.fxml"));
            Parent root1 = (Parent) loader.load();
            Stage stg = new Stage();
            stg.setTitle("Import CSV file");
            stg.setScene(new Scene(root1));
            stg.show();
        }catch (IOException e){
            System.out.println("Cant load");
        }

    }

    public void onBtBackAction(ActionEvent actionEvent) {
        Stage stageTheLabelBelongs = (Stage) Main.getPrimaryStage().getScene().getWindow();
        LoginController.getMenuStage().close();
        stageTheLabelBelongs.show();


    }
    public static Stage getMenuStage() {
        return menuStage;
    }
    private static Stage menuStage;
    private void loadPage(String absoluteName){
        try {
            Stage stage = new Stage();
            stage.setTitle("Menu");
            VBox myPane = null;
            myPane = FXMLLoader.load(getClass().getResource(absoluteName));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            this.menuStage=stage;

            MainMenuController.getLoginStage().close();
            stage.show();
        }catch (IOException e){
            Alerts.showAlert("IO Exception","Error loading new page",e.getMessage(), Alert.AlertType.ERROR);
        }

    }
}
