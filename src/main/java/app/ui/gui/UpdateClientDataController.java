package app.ui.gui;

import app.Main;
import app.controller.App;
import app.controller.business.UpdatePersonalDataController;
import app.domain.model.Client;
import app.domain.model.Company;
import app.ui.gui.util.Alerts;
import auth.UserSession;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UpdateClientDataController implements Initializable {

    @FXML
    private Button btSave;

    @FXML
    private TextField txtEmail;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtPhoneNumber;

    @FXML
    private TextField txtSex;

    private Company company;
    private UpdatePersonalDataController updatePersonalDataController=new UpdatePersonalDataController();

    @FXML
    public void onBtSaveAction(){
        try {
            String name = txtName.getText();
            String email = txtEmail.getText();
            String sex = txtSex.getText();
            Long number = Long.parseLong(txtPhoneNumber.getText());

            UserSession us = App.getInstance().getCurrentUserSession();
            Client client = company.getClientByEmail(us.getUserId().getEmail());

            client.setName(name);
            client.setEmail(email);
            client.setSex(sex);
            client.setPhoneNumber(number);

            updatePersonalDataController.updatePersonalData(client);

            Stage stageTheLabelBelongs = (Stage) LoginController.getMenuStage().getScene().getWindow();
            ClientMenuController.getClientStage().close();
            stageTheLabelBelongs.show();
        }catch (Exception e){
            Alerts.showAlert("Exception","Error saving data",e.getMessage(), Alert.AlertType.ERROR);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        this.company=App.getInstance().getCompany();
        UserSession us= App.getInstance().getCurrentUserSession();
        Client client=company.getClientByEmail(us.getUserId().getEmail());
        txtName.setText(client.getName());
        txtEmail.setText(client.getEmail());
        String number= String.valueOf(client.getPhoneNumber());
        txtPhoneNumber.setText(number);
        txtSex.setText(client.getSex());
    }

}
