package app.ui.gui;

import app.controller.App;
import app.domain.model.Client;
import app.domain.model.Test;
import app.searchClient.SearchClientByName;
import app.searchClient.SearchClientByTIN;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class HistoricalTestsController implements Initializable {

    @FXML
    private ComboBox<Client> comboBoxClient;

    @FXML
    private ComboBox<String> comboBoxOrder;

    private ObservableList<Client> obsListClient;
    private ObservableList<String> obsListMethods;

    @FXML
    private TextArea testAreaResults;

    public void onComboBoxOrderAction(){
        list();
    }

    public void onComboBoxClientAction(){
        testAreaResults.clear();

        app.controller.business.ConsultHistoricalTestsController cht = new app.controller.business.ConsultHistoricalTestsController();
        Set<Client> clientsSetTotal =cht.getClientList();
        Client client=new Client();

        for(Client c : clientsSetTotal){
            if(c.getTIFnumber()==comboBoxClient.getSelectionModel().getSelectedItem().getTIFnumber()){
                client=c;
            }
        }

        app.controller.business.ViewTestResultsController ctr = new app.controller.business.ViewTestResultsController();
        ArrayList<Test> lst =ctr.listOfTestsClient(client);


        int selectedIndex = comboBoxClient.getSelectionModel().getSelectedIndex();
        Test selectedItem = new Test();

        for(Test t : lst){
            if(t.getClient()== client){
                selectedItem=(Test) t;
                testAreaResults.appendText(t.print()+"\n\n");
            }
        }

    }

    public void list(){

        app.controller.business.ConsultHistoricalTestsController cht = new app.controller.business.ConsultHistoricalTestsController();

        Set<Client> clientsSetTotal =cht.getClientList();
        ArrayList<Client> clientsListTotal=new ArrayList<>();

        for(Client c : clientsSetTotal){
            clientsListTotal.add(c);
        }

        ArrayList<Client> clientsListResult=new ArrayList<>();

        if(comboBoxOrder.getSelectionModel().getSelectedItem().equals("Order by Name")) {
            SearchClientByName searchClientByName = new SearchClientByName();
            clientsListResult = searchClientByName.getListClientByName(clientsListTotal);
        }else{
            SearchClientByTIN searchClientByTIN=new SearchClientByTIN();
            clientsListResult=searchClientByTIN.getListClientByTin(clientsListTotal);
        }

        obsListClient = FXCollections.observableArrayList(clientsListResult);
        comboBoxClient.setItems(obsListClient);

        Callback<ListView<Client>, ListCell<Client>> factory = lv -> new ListCell<Client>() {
            @Override
            protected void updateItem(Client item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getName()+", TIN Number= "+item.getTIFnumber());
            }
        };

        comboBoxClient.setCellFactory(factory);
        comboBoxClient.setButtonCell(factory.call(null));

    }

    public void searchMethods(){
        ArrayList<String> methods=new ArrayList<>();
        methods.add("Order by Name");
        methods.add("Order by TIN");
        obsListMethods=FXCollections.observableArrayList(methods);
        comboBoxOrder.setItems(obsListMethods);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //list();
        searchMethods();
    }

}
