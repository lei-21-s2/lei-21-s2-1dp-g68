package app.algortim;

import app.controller.App;
import app.domain.model.Company;
import app.domain.model.Test;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class SearchAlgorithm implements Serializable {




    public SearchAlgorithm()  {
    }



    public Map<String, List<Integer>> getLIstbyBruteForceAlgorithm(Date beginDate, Date endDate, List<Test> testList){
        if (beginDate.after(endDate)){
            throw new IllegalArgumentException("the begin date cant be after the end Date");
        }
        Map< String, List<Integer>> result=new HashMap();
        List<String> temp=new LinkedList<>();
        int count=0;
        for (Test test: testList){
            if (!temp.contains(test.getClient().getEmail())){
                temp.add(test.getClient().getEmail());
                count++;
            }
        }
        result.put("Number of Clients:", new LinkedList<>());
        result.get("Number of Clients:").add(count);
        System.out.println("Number of Clients:"+count);
        count=0;
        for (Test test:testList) {
            if (test.getDiagnosis().getReport()==null){
                count++;
            }
        }

        result.put("Number of Test waiting for the all diagnostic:", new LinkedList<>());
        result.get("Number of Test waiting for the all diagnostic:").add(count);
        System.out.println("Number of Test waiting for the all diagnostic:"+count);
       int testsdates=0;

        long diffDays=getDateDiff(beginDate, endDate, TimeUnit.DAYS);

        result.put("Number of Test by days:", new LinkedList<>());

        for (int i = 0; i <= diffDays; i++) {
            for (Test test:testList) {
                if (getDateDiff(beginDate, test.getDateofcriaction(),TimeUnit.DAYS)==i){
                    testsdates++;

                }
            }
            result.get("Number of Test by days:").add(i, testsdates);
            testsdates=0;
        }
        //falta o resto

        testsdates=0;
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String begin=simpleDateFormat.format(beginDate);
        String end=simpleDateFormat.format(endDate);
        Period diff = Period.between(
                LocalDate.parse(begin).withDayOfMonth(1),
                LocalDate.parse(end).withDayOfMonth(1));
        String mid="";
        result.put("Number of Test by months:", new LinkedList<>());
        for (int i = 0; i <= diff.getMonths(); i++) {
            for (int j = 0; j < testList.size(); j++) {
                mid=simpleDateFormat.format(testList.get(j).getDateofcriaction());
                Period re=Period.between(
                        LocalDate.parse(begin).withDayOfMonth(1),
                        LocalDate.parse(mid).withDayOfMonth(1));
                if (re.getMonths()==i){
                    testsdates++;
                }
            }
            result.get("Number of Test by months:").add(i, testsdates);
            testsdates=0;
        }
        testsdates=0;
        long time= endDate.getTime()- beginDate.getTime();
        System.out.println();
        result.put("Number of Test by year:", new LinkedList<>());
        for (int i = 0; i <= (time/ (1000L *60*60*24*365)); i++) {
            for (Test test:testList) {
                long timea=test.getDateofcriaction().getTime()-beginDate.getTime();
                timea=(timea/ (1000L *60*60*24*365));
               if (timea==i){
                   testsdates++;
               }
            }
            result.get("Number of Test by year:").add(i, testsdates);
            testsdates=0;
        }
        result.put("Number of Test that missing result:", new LinkedList<>());
        count=0;
        for (Test test:testList) {
            if (test.getTestResult().getTrDate()==null){
                count++;
            }
        }
        result.get("Number of Test that missing result:").add(count);
        return result;
    }


    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

}
