package app;

import app.algortim.MultiplelinearRegression;
import app.ui.console.MainMenuUI;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import java.text.ParseException;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Main extends Application{

    public static void main(String[] args) throws ParseException{

       // launch(args);

       /* try {
            MainMenuUI menu = new MainMenuUI();
            menu.run();
        }
        catch( Exception e ) {
        e.printStackTrace();
        }*/

    }

    private static Scene mainScene;
    private static Stage primaryStage;

    @Override
    public void start(Stage stage) throws Exception{
        this.primaryStage = stage;
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("/app/ui/gui/MainMenu.fxml"));
        ScrollPane myPane = (ScrollPane) myLoader.      load();
        primaryStage.setTitle("Main Menu");
        Scene myScene = new Scene(myPane);
        primaryStage.setScene(myScene);
        primaryStage.show();

        stage.setScene(myScene);
        stage.show();

        primaryStage = stage;
    }

    public static Scene getMainScene() {
        return mainScene;
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }
}
