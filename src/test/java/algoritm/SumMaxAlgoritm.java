package algoritm;

import app.algortim.SumApi;
import org.junit.Assert;
import org.junit.Test;

public class SumMaxAlgoritm {
    @Test
    public void TestSum(){
        app.algortim.SumApi sumApi=new SumApi();
        int[] temp={29, -32, -9, -25, 44, 12, -61, 51, -9, 44, 74, 4};
        Assert.assertEquals(sumApi.Max(temp).length,5 );
    }
}
