package algoritm;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class SearchAlgoritm {

    @Test
    public void SearchAlgoritmgetDateDiff(){
        Date date=new Date();
        Date date1=new Date();
        Assert.assertEquals(0, app.algortim.SearchAlgorithm.getDateDiff(date1, date, TimeUnit.DAYS));
    }

    @Test(expected =IllegalArgumentException.class )
    public void SearchAlgoritmDatesDiferent() throws ParseException {
        app.algortim.SearchAlgorithm searchAlgoritm=new app.algortim.SearchAlgorithm();
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        Date date=simpleDateFormat.parse("12-04-2020");
        searchAlgoritm.getLIstbyBruteForceAlgorithm(new Date(), date, new LinkedList<>());
    }
}
