package algoritm;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MultiplelinearRegression {

    private double x[][];
    private double y[];
    @Before
    public void constr(){
         x=new double[12][3];
        x[0][0]=1;
        x[1][0]=1;
        x[2][0]=1;
        x[3][0]=1;
        x[4][0]=1;
        x[5][0]=1;
        x[6][0]=1;
        x[7][0]=1;
        x[8][0]=1;
        x[9][0]=1;
        x[10][0]=1;
        x[11][0]=1;
        x[0][1]=80;
        x[1][1]=93;
        x[2][1]=100;
        x[3][1]=82;
        x[4][1]=90;
        x[5][1]=99;
        x[6][1]=81;
        x[7][1]=96;
        x[8][1]=94;
        x[9][1]=93;
        x[10][1]=97;
        x[11][1]=95;
        x[0][2]=8;
        x[1][2]=9;
        x[2][2]=10;
        x[3][2]=12;
        x[4][2]=11;
        x[5][2]=8;
        x[6][2]=8;
        x[7][2]=10;
        x[8][2]=12;
        x[9][2]=11;
        x[10][2]=13;
        x[11][2]=11;
         y=new double[12];
        y[0]=2256;
        y[1]=2340;
        y[2]=2426;
        y[3]=2293;
        y[4]=2330;
        y[5]=2368;
        y[6]=2250;
        y[7]=2409;
        y[8]=2364;
        y[9]=2379;
        y[10]=2440;
        y[11]=2364;
    }

}
