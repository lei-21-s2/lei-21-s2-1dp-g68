package app.domain.model;

import app.domain.model.TestType;
import org.junit.Assert;
import org.junit.Test;

public class TestTestType {
    private TestType testType;

    @Test
    public void TestGetName(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        Assert.assertEquals(testType.getName(), "ola");
    }
    @Test
    public void TestgetCollectionMethod(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        Assert.assertEquals(testType.getCollectionMethod(), "ola");
    }

    @Test
    public void TestgetModule(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        Assert.assertEquals(testType.getModule(), "ola");
    }
}
