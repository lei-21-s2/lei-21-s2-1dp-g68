package app.domain.model;

import app.domain.model.Doctor;
import app.domain.model.Employee;
import org.junit.Assert;
import org.junit.Test;

public class TestDoctor {

    private Doctor doctor;
    @Test
    public void TestConstrutor(){

        doctor=new Doctor("123", "João", "rua do isep", "987654321",
                "antoniomsbarros@gmail.com", "12356", "145");
        Assert.assertEquals(doctor.getDoctorIndexNumber(), "145");
    }
    @Test
    public void TestSetDoctor(){
        doctor=new Doctor("123", "João", "rua do isep", "987654321",
                "antoniomsbarros@gmail.com", "12356", "145");
        doctor.setDoctorIndexNumber("541");
        Assert.assertEquals(doctor.getDoctorIndexNumber(), "541");
    }

}
