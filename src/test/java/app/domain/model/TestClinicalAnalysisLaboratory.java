package app.domain.model;

import app.domain.model.ClinicalAnalysisLaboratory;
import app.domain.model.TestType;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class TestClinicalAnalysisLaboratory {

    private ClinicalAnalysisLaboratory clinicalAnalysisLaboratory;
    private TestType testType;


    @org.junit.Test
    public void TestConstrutor(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        Assert.assertEquals(clinicalAnalysisLaboratory.getLaboratoryId(), 12312);
    }

    @org.junit.Test
    public void TestConstrutor1(){
        List<TestType> tt=new ArrayList<>();
        testType=new TestType("name1","cod1","metodo1", "module1");
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        Assert.assertEquals(clinicalAnalysisLaboratory.getName(), "ola");
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestCheckNameRules(){
        List<TestType> tt=new ArrayList<>();
        testType=new TestType("name1","cod1","metodo1", "module1");
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "", "ola",
                "987654321", 123, tt);
    }
    @Test(expected = IllegalArgumentException.class)
    public void TestCheckNameRulesNameGreater10(){
        List<TestType> tt=new ArrayList<>();
        testType=new TestType("name1","cod1","metodo1", "module1");
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "Laboratorio", "ola",
                "987654321", 123, tt);
    }

    @Test(expected = IllegalArgumentException.class)
    public void TestcheckLaboratoryIdRules(){
        List<TestType> tt=new ArrayList<>();
        testType=new TestType("name1","cod1","metodo1", "module1");
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(-1, "Loratorio", "ola",
                "987654321", 123, tt);
    }

    @Test
    public void TestsetLaboratoryId(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setLaboratoryId(321212);
        Assert.assertEquals(clinicalAnalysisLaboratory.getLaboratoryId(), 321212);
    }
    @Test
    public void TestsetName(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setName("Name do clinical analysis");
        Assert.assertEquals(clinicalAnalysisLaboratory.getName(), "Name do clinical analysis");
    }
    @Test
    public void TestGetaddress(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);

        Assert.assertEquals(clinicalAnalysisLaboratory.getAddress(), "ola");
    }
    @Test
    public void TestSetAddress(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setAddress("olaola");
        Assert.assertEquals(clinicalAnalysisLaboratory.getAddress(), "olaola");
    }
    @Test
    public void TestGetPhoneNumber(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);

        Assert.assertEquals(clinicalAnalysisLaboratory.getPhoneNumber(), "987654321");
    }
    @Test
    public void TestsetPhoneNumber(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setPhoneNumber("123456789");
        Assert.assertEquals(clinicalAnalysisLaboratory.getPhoneNumber(), "123456789");
    }
    @Test
    public void TestgetTinNumber(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        Assert.assertEquals(clinicalAnalysisLaboratory.getTinNumber(), 123);
    }
    @Test
    public void setTinNumber(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setTinNumber(1231231);
        Assert.assertEquals(clinicalAnalysisLaboratory.getTinNumber(), 1231231);
    }
    @Test
    public void getTesttype(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);

        Assert.assertEquals(clinicalAnalysisLaboratory.getTesttype().size(), 1);
    }
    @Test
    public void setTesttype(){
        testType=new TestType("name1","cod1","metodo1", "module1");
        List<TestType> tt=new ArrayList<>();
        tt.add(testType);
        clinicalAnalysisLaboratory=new ClinicalAnalysisLaboratory(12312, "ola", "ola",
                "987654321", 123, tt);
        clinicalAnalysisLaboratory.setTesttype(new LinkedList<>());
        Assert.assertEquals(clinicalAnalysisLaboratory.getTesttype().size(), 0);
    }
}
