package app.domain.model;

import app.domain.model.TestResult;
import com.example2.EMRefValue;
import org.junit.Assert;

import java.time.LocalDate;
import java.util.Date;

public class TestTestResult {

    private TestResult testResult1;
    private TestResult testResult2;

    @org.junit.Test
    public void TestConstrutor1(){
        LocalDate date1=LocalDate.now();
        LocalDate date2=date1.minusDays(3);

        Date dateEmr1=new Date(2021/06/16);
        Date dateEmr2=new Date(2021/07/16);

        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        EMRefValue emRefValue2=new EMRefValue("ESR00","g/L",0.0,9.0,dateEmr2);

        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);
        testResult2=new TestResult(date2,"6.0","g/L",emRefValue2);

        Assert.assertNotEquals(testResult1,testResult2);
    }

    @org.junit.Test
    public void TestConstrutor2(){
        LocalDate date1=LocalDate.now();
        LocalDate date2=date1.minusDays(3);
        testResult1=new TestResult(date1);
        testResult2=new TestResult(date2);
        Assert.assertNotEquals(testResult1,testResult2);
    }

    @org.junit.Test
    public void testGetDate(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        Assert.assertEquals(testResult1.getTrDate(),date1);
    }

    @org.junit.Test
    public void testSetDate(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        LocalDate date2=date1.minusDays(2);

        testResult1.setTrDate(date2);

        Assert.assertEquals(testResult1.getTrDate(),date2);
    }

    @org.junit.Test
    public void testGetResult(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        Assert.assertEquals(testResult1.getResult(),"3.0");
    }

    @org.junit.Test
    public void testSetResult(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        testResult1.setResult("4.0");

        Assert.assertEquals(testResult1.getResult(),"4.0");
    }

    @org.junit.Test
    public void testGetMetric(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        Assert.assertEquals(testResult1.getMetric(),"g/L");
    }

    @org.junit.Test
    public void testSetMetric(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        testResult1.setMetric("10e9L");

        Assert.assertEquals(testResult1.getMetric(),"10e9L");
    }

    @org.junit.Test
    public void testGetValues(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        Assert.assertEquals(testResult1.getValues(),emRefValue1);
    }

    @org.junit.Test
    public void testSetValues(){
        LocalDate date1=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        testResult1=new TestResult(date1,"3.0","g/L",emRefValue1);

        Date dateEmr2=new Date(2021/07/16);
        EMRefValue emRefValue2=new EMRefValue("ESR00","g/L",0.0,9.0,dateEmr2);

        testResult1.setValues(emRefValue2);

        Assert.assertEquals(testResult1.getValues(),emRefValue2);
    }

}
