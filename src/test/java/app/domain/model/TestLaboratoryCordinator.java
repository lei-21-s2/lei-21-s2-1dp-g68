package app.domain.model;

import app.domain.model.LaboratoryCoordinator;
import app.domain.model.Receptionist;
import org.junit.Assert;
import org.junit.Test;

public class TestLaboratoryCordinator {
    private LaboratoryCoordinator laboratoryCoordinator;


    @Test
    public void TestConstrutor(){
        laboratoryCoordinator=new LaboratoryCoordinator("12312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235", "1231");
        Assert.assertNotEquals(laboratoryCoordinator, new LaboratoryCoordinator("123213312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235", "2312"));
    }
    @Test
    public void TestGetCoordinatorIndexNumber(){
        laboratoryCoordinator=new LaboratoryCoordinator("12312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235", "1231");
        Assert.assertEquals(laboratoryCoordinator.getCoordinatorIndexNumber(), "1231");
    }
    @Test
    public void TestSetCoordinatorIndexNumber(){
        laboratoryCoordinator=new LaboratoryCoordinator("12312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235", "1231");
        laboratoryCoordinator.setCoordinatorIndexNumber("123");
        Assert.assertEquals(laboratoryCoordinator.getCoordinatorIndexNumber(), "123");
    }
}
