package app.domain.model;

import app.adapter.SumAdpter;
import app.domain.model.Company;
import auth.UserSession;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class TestCompany {
    private Company company;




    @Test
    public void TestConstrutor(){
        company=new Company("desgnation","abc","add");
        Assert.assertNotEquals(company, new Company("dweqa","adf","adf"));
    }

    @Test
    public void TestgetDesignation(){
        company=new Company("desgnation","abc","add");
        Assert.assertEquals(company.getDesignation(), "desgnation");
    }
@Test(expected = IllegalArgumentException.class)
    public void TestCOnstrutor(){
        company=new Company("","ola","ika");
}
    @Test
    public void TestConstrutor1(){
        company=new Company();
        Assert.assertEquals(0, company.getClientList().size());
    }

    @Test
    public void AuthFacade(){
        company=new Company("desgnation","abc","add");
        Assert.assertNotEquals(1, company.getAuthFacade());
    }
    @Test
    public void AddClient(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addClient(new Client("Antonio","antoniomsbarros@gmail.com", "masculino",new Date(),2131,123L, 1233L ,987654321L  )));
    }
    @Test
    public void AddDoctor(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addDoctor(new Doctor("123", "Antonio", "rua do isep",
                "987654321", "antoniomsbarros@gmail.com", "123", "3213")));
    }
    @Test
    public void AddSpecialistDoctor(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addSpecialistDoctor(new SpecialistDoctor("123", "Antonio", "rua do isep",
                "987654321", "antoniomsbarros@gmail.com", "123", "3213")));
    }
    @Test
    public void AddReceptionist(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addReceptionist(new Receptionist("123", "Antonio", "rua do isep",
                "987654321", "antoniomsbarros@gmail.com", "123")));
    }
    @Test
    public void addLaboratoryCoordinator(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addLaboratoryCoordinator(new LaboratoryCoordinator("123", "Antonio", "rua do isep",
                "987654321", "antoniomsbarros@gmail.com", "123", "123")));
    }
   /* @Test
    public void addClinicalChemistryTechnologist(){
        company=new Company("desgnation","abc","add");
    Assert.assertTrue(company.addClinicalChemistryTechnologist(new ClinicalChemistryTechnologist("123", "Antonio", "rua do isep",
        "987654321", "antoniomsbarros@gmail.com", "123")));
    }*/
    @Test
    public void  addClinicalAnalysisLaboratory(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addClinicalAnalysisLaboratory(new ClinicalAnalysisLaboratory(123 ,"Antonio", "rua do isep",
                "987654321", 123, new LinkedList<>())));
    }
    @Test
    public void  addTypeTest(){
        company=new Company("desgnation","abc","add");
    Assert.assertTrue(company.addTypeTest(new TestType()));
    }
    @Test
    public void getTestTypeList(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getTestTypeList().size()==0);
    }
    @Test
    public void addParameterCategory(){
        company=new Company("desgnation","abc","add");
        TestType testType=new TestType();
        Assert.assertTrue(company.addParameterCategory(new ParameterCategory("12334", "oal", "2131", testType)));
    }
    @Test
    public void getParameterCategoryList(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getParameterCategoryList().size()==0);
    }
    @Test
    public void addParameter(){
        company=new Company("desgnation","abc","add");
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("12334", "oal", "2131", testType);
        Assert.assertTrue(company.addParameter(new Parameter("12334", "oal", "2131", parameterCategory)));
    }
    @Test
    public void AddTest(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.addTest(new app.domain.model.Test()));
    }

    @Test
    public void getClientListTest(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getClientList().size()==0);
    }

    @Test
    public void getTypeTestList(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getTestTypeList().size()==0);
    }
    @Test
    public void  getParameterList(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getParameterList().size()==0);
    }
    @Test
    public void  getTestList(){
        company=new Company("desgnation","abc","add");
        Assert.assertTrue(company.getTestList().size()==0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addSampleTest(){
        company=new Company("desgnation","abc","add");
        company.addSample("code","email",new Sample(new Date(21-05-01),"barcode"));
    }

    @Test
    public void getTestBetweenDatesTest(){
        company=new Company("desgnation","abc","add");
        List<app.domain.model.Test> testList11=new LinkedList<>();
        Assert.assertEquals(company.getTestbetweenDates(new Date(18-06-2020),new Date(18-07-2021)),testList11);
    }

    @Test
    public void getSumTest(){
        company=new Company("desgnation","abc","add");
        Sum sum=new SumAdpter();
        Assert.assertTrue(company.getSum()!=sum);
    }

    @Test
    public void getTestByCodeTest(){
        company=new Company("desgnation","abc","add");
        app.domain.model.Test t=null;
        Assert.assertEquals(company.getTestByCode("hello"),t);
    }

    @Test
    public void editClientTest(){
        company=new Company("desgnation","abc","add");
        app.domain.model.Client c=new Client();
        Assert.assertFalse(company.editClient(c,0));
    }

    @Test
    public void getClientByEmailTest(){
        company=new Company("desgnation","abc","add");
        app.domain.model.Client c=null;
        Assert.assertEquals(company.getClientByEmail("email"),c);
    }

    @Test
    public void listTestClientTest(){
        company=new Company("desgnation","abc","add");
        ArrayList<app.domain.model.Test> cTests =  new ArrayList<app.domain.model.Test>();
        Assert.assertEquals(company.listTestClient(new UserSession()),cTests);
    }

    @Test
    public void listTestClientTest1(){
        company=new Company("desgnation","abc","add");
        ArrayList<app.domain.model.Test> cTests =  new ArrayList<app.domain.model.Test>();
        Assert.assertEquals(company.listTestClient1(new Client()),cTests);
    }

}



