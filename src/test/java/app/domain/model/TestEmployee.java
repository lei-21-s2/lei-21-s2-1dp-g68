package app.domain.model;

import app.domain.model.Employee;
import org.junit.Assert;
import org.junit.Test;

public class TestEmployee {

    private Employee employee;

    @Test
    public void TestGets(){
        employee=new Employee("123", "Antonio Barros","rua do isep",
                "987654321", "antoniomsbarros@gmail.com","134" );
        Assert.assertEquals(employee.getEmployeeID(), "123");
        Assert.assertEquals(employee.getName(), "Antonio Barros");
        Assert.assertEquals(employee.getAddress(), "rua do isep");
        Assert.assertEquals(employee.getPhoneNumber(), "987654321");
        Assert.assertEquals(employee.getEmail(), "antoniomsbarros@gmail.com");
        Assert.assertEquals(employee.getSocCode(), "134");
    }
    @Test(expected = IllegalArgumentException.class)
    public void TestSetName(){
        employee=new Employee("123", "Antonio Barros Manuel da Silvaqwertyuiopasdfghjklçzcvbnmqertyuiopasdfghjklzxcvbnm,1234569789 ","rua do isep",
                "987654321", "antoniomsbarros@gmail.com","134" );

    }

}
