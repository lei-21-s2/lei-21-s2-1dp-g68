package app.domain.model;

import app.domain.model.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class TestClient {

	private Client valid;
	private Client invalidClient;

	@Before
	public void setUp ( ) throws Exception {
		valid = new Client( "Carlos", "carlos@gmail.com", "male",
				new Date( new Date().getTime() ), 124219,
				215051851289L, 42387112894L, 934111110L  );

	}

	@Test
	public void testGetNameInvalid ( ) {

		valid.setName( "Joao" );
		Assert.assertNotEquals(  "Carlos", valid.getName() );
	}

	@Test ( expected = IllegalArgumentException.class )
	public void testSetNameInvalid ( ) {
		valid.setName( null );
	}

	@Test
	public void testGetEmailInvalid ( ) {

		valid.setEmail( "joao@gmail.com" );
		Assert.assertNotEquals(  "carlos@gmail.com", valid.getEmail() );
	}

	@Test
	public void testGetSexInvalid ( ) {

		valid.setSex( "female" );
		Assert.assertNotEquals(  "male", valid.getSex() );
	}

	@Test
	public void testGetSexValid ( ) {

		Assert.assertNotEquals(  "female", valid.getSex() );
	}

	@Test
	public void testGetCitizenNumberInvalid ( ) {

		valid.setCitizenCardNumber( 1 );
		Assert.assertNotEquals( 124219 , valid.getCitizenCardNumber() );
	}

	@Test
	public void testGetCitizenNumberValid ( ) {

		Assert.assertNotEquals(  1, valid.getCitizenCardNumber() );
	}
	@Test
	public void construtor(){
		Assert.assertNotEquals(valid, new Client());
	}
	@Test
	public void getBirthDate(){
		Date date=new Date( new Date().getTime() );
		valid = new Client( "Carlos", "carlos@gmail.com", "male",
				date, 124219,
				215051851289L, 42387112894L, 934111110L  );
		Assert.assertEquals(date, valid.getBirthDate());
	}
	@Test
	public void getTIFnumber(){
		Assert.assertEquals(42387112894L,valid.getTIFnumber());
	}
	@Test
	public void getPhoneNumber(){
		Assert.assertEquals(valid.getPhoneNumber(), 934111110L);
	}
	@Test
	public void toString1(){
		Assert.assertNotEquals("Client{name='Carlos', email='carlos@gmail.com', sex='male', birthDate=Thu Jun 17 21:05:54 BST 2021, citizenCardNumber=124219, NHS_number=215051851289, TIF_number=42387112894, phoneNumber=9341111110}", valid.toString());
	}
}
