package app.domain.model;

import app.domain.model.ParameterCategory;
import app.domain.model.TestType;
import org.junit.Assert;
import org.junit.Test;

public class TestParameterCategory {
    private ParameterCategory parameterCategory;

    @Test(expected =IllegalArgumentException.class )
    public void TestCheckCodeRules(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("", "ola","2313", testType );
    }
    @Test(expected =IllegalArgumentException.class )
    public void TestCheckcodeRoles1(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("1", "ola","2313", testType );
    }
    @Test(expected =IllegalArgumentException.class )
    public void TestCheckcodeRoles2(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("123456789", "ola","2313", testType );
    }
    @Test
    public void TestGetCode(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        Assert.assertEquals(parameterCategory.getCode(), "12345678");
    }
    @Test
    public void TestSetCode(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        parameterCategory.setCode("12345");
        Assert.assertEquals(parameterCategory.getCode(), "12345");
    }
    @Test
    public void TestGetDescription(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        Assert.assertEquals(parameterCategory.getDescription(), "ola");
    }
    @Test
    public void TestSetDescription(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        parameterCategory.setDescription("ola1");
        Assert.assertEquals(parameterCategory.getDescription(), "ola1");
    }
    @Test
    public void TestGetNhsId(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        Assert.assertEquals(parameterCategory.getNhsId(), "2313");
    }
    @Test
    public void TestSetNhsId(){
        TestType testType=new TestType();
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        parameterCategory.setNhsId("123");
        Assert.assertEquals(parameterCategory.getNhsId(), "123");
    }
    @Test
    public void TestGetTestType(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );

        Assert.assertEquals(parameterCategory.getTestType().getCode(), testType.getCode());
    }
    @Test
    public void TestSetTestType(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );
        TestType testType1=new TestType("qer", "321", "fadf", "abc");
        parameterCategory.setTestType(testType1);
        Assert.assertEquals(parameterCategory.getTestType().getCode(), testType1.getCode());
    }
    
    /*@Test
    public void TestToString(){
        TestType testType=new TestType("ola", "123", "ola", "ola");
        parameterCategory=new ParameterCategory("12345678", "ola","2313", testType );

        Assert.assertEquals("ParameterCategory{code='12345678', description='ola', nhsId='2313', testType=TestType{name='ola'}}", parameterCategory.toString());
    }

     */
}
