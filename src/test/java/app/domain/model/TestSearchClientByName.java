package app.domain.model;

import app.searchClient.SearchClientByName;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestSearchClientByName {

    @Test
    public void searchClientByName ( ) {

        Client client1=new Client( "Carlos", "carlos@gmail.com", "male",
                new Date( new Date().getTime() ), 124219,
                215051851289L, 42387112894L, 934111110L );

        Client client2=new Client("Joao", "joao@gmail.com", "male",
                new Date( new Date().getTime() ), 124219,
                215051851289L, 42387112894L, 934111110L );

        ArrayList<Client> clientList=new ArrayList<>();
        clientList.add(client2);
        clientList.add(client1);

        SearchClientByName searchClientByName=new SearchClientByName();
        ArrayList<Client> result=searchClientByName.getListClientByName(clientList);

        Assert.assertEquals(clientList.get(0), result.get(0) );

    }


}
