package app.domain.model;

import app.domain.model.SpecialistDoctor;
import org.junit.Assert;
import org.junit.Test;

public class TestSpecialDoctor {
    private SpecialistDoctor specialistDoctor;

    @Test
    public void TestConstructor(){
           specialistDoctor=new SpecialistDoctor("123134", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                   "123123","1327") ;
        Assert.assertNotEquals(specialistDoctor, new SpecialistDoctor("123132314", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "123123","1327"));
    }
    @Test
    public void TestSetDoctorIndexNumber(){
        specialistDoctor=new SpecialistDoctor("123134", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "123123","1327") ;
        specialistDoctor.setDoctorIndexNumber("3123");
        Assert.assertEquals(specialistDoctor.getDoctorIndexNumber(), "3123");
    }
}
