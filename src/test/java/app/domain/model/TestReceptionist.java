package app.domain.model;

import app.domain.model.Receptionist;
import app.domain.model.Tech;
import org.junit.Assert;
import org.junit.Test;

public class TestReceptionist {
    private Receptionist receptionist;

    @Test
    public void TestConstrutor(){
        receptionist=new Receptionist("12312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235");
        Assert.assertNotEquals(receptionist, new Receptionist("123213312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235"));
    }
}
