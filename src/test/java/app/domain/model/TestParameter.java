package app.domain.model;

import app.domain.model.*;
import com.example2.EMRefValue;
import org.junit.Assert;

import java.time.LocalDate;
import java.util.Date;


public class TestParameter {

    private Parameter parameter1;
    private Parameter parameter2;

    @org.junit.Test
    public void TestConstrutor(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);
        parameter2=new Parameter("code2","name2","description2",parameterCategory);

        Assert.assertNotEquals(parameter1,parameter2);
    }

    @org.junit.Test
    public void testGetCode(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        Assert.assertEquals(parameter1.getCode(),"code1");
    }

    @org.junit.Test
    public void testSetCode(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        parameter1.setCode("code2");

        Assert.assertEquals(parameter1.getCode(),"code2");
    }

    @org.junit.Test
    public void testGetName(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        Assert.assertEquals(parameter1.getName(),"name1");
    }

    @org.junit.Test
    public void testSetName(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        parameter1.setName("newName");

        Assert.assertEquals(parameter1.getName(),"newName");
    }

    @org.junit.Test
    public void testGetDescription(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        Assert.assertEquals(parameter1.getShortDescritpion(),"description1");
    }

    @org.junit.Test
    public void testSetDescription(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        parameter1.setShortDescritpion("newDescription");

        Assert.assertEquals(parameter1.getShortDescritpion(),"newDescription");
    }

    @org.junit.Test
    public void testGetParameterCategory(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        Assert.assertEquals(parameter1.getCategory(),parameterCategory);
    }

    @org.junit.Test
    public void testSetParameterCategory(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory1=new ParameterCategory("code", "description","2313", testType );
        ParameterCategory parameterCategory2=new ParameterCategory("code2", "description2","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory1);

        parameter1.setCategory(parameterCategory2);

        Assert.assertEquals(parameter1.getCategory(),parameterCategory2);
    }

    @org.junit.Test
    public void testAddTestResult(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory1=new ParameterCategory("code", "description","2313", testType );
        ParameterCategory parameterCategory2=new ParameterCategory("code2", "description2","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory1);

        LocalDate date=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);

        Assert.assertTrue(parameter1.addTestResult(date,"3.0","g/L",emRefValue1)==true);
    }

    @org.junit.Test
    public void testGetTestResult(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        LocalDate date=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        parameter1.addTestResult(date,"3.0","g/L",emRefValue1);

        TestResult testResult=new TestResult(date,"3.0","g/L",emRefValue1);

        Assert.assertEquals(parameter1.getTestResult().getResult(),testResult.getResult());
    }

    @org.junit.Test
    public void testSetTestResult(){
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        parameter1=new Parameter("code1","name1","description1",parameterCategory);

        LocalDate date=LocalDate.now();
        Date dateEmr1=new Date(2021/06/16);
        EMRefValue emRefValue1=new EMRefValue("ESR00","g/L",2.0,5.0,dateEmr1);
        parameter1.addTestResult(date,"3.0","g/L",emRefValue1);

        TestResult testResult=new TestResult(date,"4.0","g/L",emRefValue1);

        parameter1.setTestResult(testResult);

        Assert.assertEquals(parameter1.getTestResult().getResult(),testResult.getResult());
    }

}
