package app.domain.model;

import app.domain.model.Sample;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class TestSample {

	private Sample sample;
	private Sample sample1;

	@org.junit.Test
	public void TestConstrutor ( ) {

		sample = new Sample( new Date( ), "1" );
		sample1 = new Sample( new Date( ), "2" );
		Assert.assertNotEquals( sample, sample1 );
	}

	@org.junit.Test
	public void TestSetDate ( ) {

		sample = new Sample( new Date( ), "1" );
		Date date1 = new Date( );
		sample.setSampleDate( date1 );
		Assert.assertEquals( date1, sample.getSampleDate( ) );
	}

	@org.junit.Test
	public void TestGetDAte ( ) {

		Date date1 = new Date( );
		sample = new Sample( date1, "1" );
		Assert.assertEquals( date1, sample.getSampleDate( ) );
	}

	@org.junit.Test
	public void TestSetBarcode ( ) {

		Date date1 = new Date( );
		sample = new Sample( date1, "1" );
		sample.setBarcode( "2" );
		Assert.assertNotEquals( sample.getBarcode( ), "1" );
	}

	@Test
	public void TestToString ( ) {

		Date date1 = new Date( );
		sample = new Sample( date1, "1" );

		String str = sample.toString( );
		Assert.assertEquals( sample.toString( ), str );
	}

}