package app.domain.model;

import app.domain.model.*;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;


import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class TestTest {

    private Test test;
    private TestType type;
    private Client cliente;

    public TestTest(){

    }

    @org.junit.Test
    public void TestGetInternalCode(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals("123456789123",test.getInternalCode());
    }

    @org.junit.Test
    public void TestSetInternalCode(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setInternalCode("111111111111");
        Assert.assertEquals("111111111111",test.getInternalCode());
    }

    @org.junit.Test
    public void TestGetNHSCode(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals("123456789012",test.getNhsCode());
    }

    @org.junit.Test
    public void TestSetNHSCode(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setNhsCode("111111111111");
        Assert.assertEquals("111111111111",test.getNhsCode());
    }

    @org.junit.Test
    public void TestGetType(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(type,test.getType());
    }

    @org.junit.Test
    public void TestSetType(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        TestType type2 = new TestType("type2","444","collection2","modelo2");
        test.setType(type2);
        Assert.assertEquals(type2,test.getType());
    }

    @org.junit.Test
    public void TestGetParameterCategoryList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(pcList,test.getPcList());
    }

    @org.junit.Test
    public void TestSetParameterCategoryList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Set<ParameterCategory> pcList2 = new HashSet<>();
        ParameterCategory pc2 = new ParameterCategory("2222222","teste2","2222",type);
        pcList2.add(pc2);
        test.setPcList(pcList2);
        Assert.assertEquals(pcList2,test.getPcList());
    }

    @org.junit.Test
    public void TestGetParameterList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(pList,test.getpList());
    }

    @org.junit.Test
    public void TestSetParameterList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Set<Parameter> pList2 = new HashSet<>();
        Parameter p2 = new Parameter("33333","nome2","descricao2",pc1);
        pList2.add(p);
        test.setpList(pList2);
        Assert.assertEquals(pList2,test.getpList());
    }


    @org.junit.Test
    public void TestGetClient(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(cliente,test.getClient());
    }

    @org.junit.Test
    public void TestSetClient(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Client cleinte2 =  new Client("Joao","joao@gmail.com","Male",data,1234, 12345L, 123455L, 1234565L);
        test.setClient(cleinte2);
        Assert.assertEquals(cleinte2,test.getClient());
    }

    @org.junit.Test
    public void TestInternalCodeLenght13(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);

        try{
            this.test= new Test("1234567890123","123456789012",type,pcList,pList,cliente);
        }catch(IllegalArgumentException ex){
            Assertions.assertEquals("Internal Code length must be 12",ex.getMessage());

        }
    }

    @org.junit.Test
    public void TestInternalCodeLenght9(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);

        try{
            this.test= new Test("123456789","123456789012",type,pcList,pList,cliente);
        }catch(IllegalArgumentException ex){
            Assertions.assertEquals("Internal Code length must be 12",ex.getMessage());

        }
    }
    @org.junit.Test
    public void getClient(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(cliente, test.getClient());
    }
    @org.junit.Test
    public void setClient(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setClient(cliente);
        Assert.assertEquals(cliente, test.getClient());
    }
    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setCliente1(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setClient(null);
    }
    @org.junit.Test
    public void getSampleList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertEquals(0, test.getSampleList().size());
    }
    @org.junit.Test
    public void setSampleList(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setSampleList(new LinkedList<>());
        Assert.assertEquals(0, test.getSampleList().size());
    }
    @org.junit.Test
    public void  getDiagnosis(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Assert.assertNotEquals(test.getDiagnosis(),null);
    }
    @org.junit.Test(expected = IllegalArgumentException.class)
    public void setDiagnosis(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        test.setDiagnosis(null);
    }
    @org.junit.Test
    public void setDiagnosis1(){
        this.type= new TestType("type1","123","collection1","modelo1");
        Date data = new Date(2001,12,12);
        this.cliente= new Client("Bruno","bruno@gmail.com","Male",data,123, 1234L, 12345L, 123456L);
        Set<ParameterCategory> pcList = new HashSet<>();
        ParameterCategory pc1 = new ParameterCategory("1234567","ola","1234",type);
        pcList.add(pc1);
        Set<Parameter> pList = new HashSet<>();
        Parameter p = new Parameter("12345","nome","descricao",pc1);
        pList.add(p);
        this.test= new Test("123456789123","123456789012",type,pcList,pList,cliente);
        Diagnosis diagnosis=new Diagnosis();
        test.setDiagnosis(diagnosis);
        Assert.assertEquals(diagnosis, test.getDiagnosis());
    }
}
