package app.domain.model;

import app.domain.model.Diagnosis;
import org.junit.Assert;

import java.time.LocalDate;

public class TestDiagnosis {

    private Diagnosis diagnosis1;
    private Diagnosis diagnosis2;

    @org.junit.Test
    public void TestConstrutor(){
        LocalDate date = LocalDate.now();
        diagnosis1=new Diagnosis("diagnosis 1",date);
        diagnosis2=new Diagnosis("diagnosis 2",date);
        Assert.assertNotEquals(diagnosis1,diagnosis2);
    }

    @org.junit.Test
    public void testSetReport(){
        LocalDate date = LocalDate.now();
        diagnosis1=new Diagnosis("diagnosis 1",date);

        String newDiagnosis="new diagnosis";
        diagnosis1.setReport(newDiagnosis);

        Assert.assertEquals(newDiagnosis,diagnosis1.getReport());
    }

    @org.junit.Test
    public void testGetReport(){
        LocalDate date = LocalDate.now();
        String diagnosis="diagnosis 1";
        diagnosis1=new Diagnosis(diagnosis,date);

        Assert.assertEquals(diagnosis,diagnosis1.getReport());
    }

    @org.junit.Test
    public void testCountWords(){
        LocalDate date = LocalDate.now();
        diagnosis1=new Diagnosis("diagnosis1",date);

        String words="one two three";

        int result=diagnosis1.countWords(words);

        Assert.assertEquals(result,3);
    }

    @org.junit.Test
    public void testGetDate(){
        LocalDate date = LocalDate.now();
        String diagnosis="diagnosis 1";
        diagnosis1=new Diagnosis(diagnosis,date);

        Assert.assertEquals(diagnosis1.getDate(),date);
    }

    @org.junit.Test
    public void testSetDate(){
        LocalDate date = LocalDate.now();
        String diagnosis="diagnosis 1";
        diagnosis1=new Diagnosis(diagnosis,date);

        LocalDate date1=date.minusDays(2);

        diagnosis1.setDate(date1);

        Assert.assertEquals(diagnosis1.getDate(),date1);
    }

}
