package app.domain.model;

import app.searchClient.SearchClientByName;
import app.searchClient.SearchClientByTIN;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

public class TestSearchClientByTIN {

    @Test
    public void searchClientByTIN ( ) {

        Client client1=new Client( "Carlos", "carlos@gmail.com", "male",
                new Date( new Date().getTime() ), 124219,
                215051851289L, 123, 934111110L );

        Client client2=new Client("Joao", "joao@gmail.com", "male",
                new Date( new Date().getTime() ), 124219,
                215051851289L, 456, 934111110L );

        ArrayList<Client> clientList=new ArrayList<>();
        clientList.add(client2);
        clientList.add(client1);

        SearchClientByTIN searchClientByName=new SearchClientByTIN();
        ArrayList<Client> result=searchClientByName.getListClientByTin(clientList);

        Assert.assertEquals(clientList.get(0), result.get(0) );

    }

}
