package app.domain.model;

import app.domain.model.Tech;
import org.junit.Assert;
import org.junit.Test;

public class TestTech {
    private Tech tech;

    @Test
    public void TestConstructor(){
        tech=new Tech("12312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235");
        Assert.assertNotEquals(tech, new Tech("123213312", "Sofia", "rua do isep", "987654321", "antoniomsbarros@gmail.com",
                "1235"));
    }
}
