package Adpter;

import app.domain.model.Client;
import app.domain.model.Parameter;
import app.domain.model.ParameterCategory;
import app.domain.model.TestType;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SearchAlgoritmAdpter {


    @Test
    public void getLIstbyBruteForceAlgorithm(){
        app.adapter.SearchAlgoritmAdpter searchAlgoritmAdpter=new app.adapter.SearchAlgoritmAdpter();
        Client client=new Client("António Barros", "antoniomsbarros@gmail.com","male", new Date(), 123, 3211L, 121L, 987654321L );

        Client client1 = new Client("Rui Pedro","ruip@manylabs.pt","male",new Date(),1234,1234L,122L,917806336L);

        TestType tt=new TestType("blood test", "1", "needle","app.adapter.ExternalModule2");

        ParameterCategory parameterCategory=new ParameterCategory("ESR00","pc", "123" ,tt);
        Set<ParameterCategory> parameterCategories=new HashSet<>();
        parameterCategories.add(parameterCategory);

        Parameter parameter=new Parameter("ESR00","name", "shortDescription",parameterCategory);
        Set<Parameter> parameters=new HashSet<>();
        parameters.add(parameter);

        app.domain.model.Test test=new app.domain.model.Test("000000000000","123456789abc",tt,parameterCategories,parameters,client);
        app.domain.model.Test test4=new app.domain.model.Test("000000000004","123456789xbc",tt,parameterCategories,parameters,client);
        app.domain.model.Test test5=new app.domain.model.Test("000000000005","123456789zbc",tt,parameterCategories,parameters,client);
        List<app.domain.model.Test> testList=new LinkedList<>();
        testList.add(test);
        testList.add(test4);
        testList.add(test5);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");
        Date date= null;
        try {
            date = simpleDateFormat.parse("16-06-2021");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(6, searchAlgoritmAdpter.getLIstbyBruteForceAlgorithm(date, new Date(), testList).size());
    }
}
