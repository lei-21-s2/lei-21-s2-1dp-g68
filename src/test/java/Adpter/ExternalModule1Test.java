package Adpter;

import app.adapter.ExternalModule1;
import app.domain.model.Company;
import app.domain.model.Parameter;
import app.domain.model.ParameterCategory;
import app.domain.model.TestType;
import auth.UserSession;
import com.example2.EMRefValue;
import net.sourceforge.barbecue.output.OutputException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class ExternalModule1Test {

    @Test
    public void externalModule1() throws OutputException, IOException {
        TestType testType=new TestType();
        ParameterCategory parameterCategory=new ParameterCategory("code", "description","2313", testType );

        Parameter parameter1=new Parameter("code1","name1","description1",parameterCategory);

        EMRefValue emRefValue=new EMRefValue("cod","metric",2.0,3.0,new Date(18-05-2020));

        ExternalModule1 externalModule1=new ExternalModule1();

        Assert.assertFalse(externalModule1.getReferenceValues(parameter1)==emRefValue);
    }

}
