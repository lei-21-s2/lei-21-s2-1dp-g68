# US4: As a receptionist of the laboratory, I intend to register a test to be performed to a registered client.



## 1. Requirements Engineering

### 1.1. User Story Description

As a receptionist of the laboratory, I intend to register a test to be performed to a
registered client.


### 1.2. Customer Specifications and Clarifications 


### 1.3. Acceptance Criteria

The receptionist must select the parameters to be analysed from all possible parameters in accordance with the test type.

### 1.4. Found out Dependencies

Must have Test Type, Parameter, Parameter Category and Client US's done.

### 1.5 Input and Output Data

All atributtes must be filled.
Need to show clients, test types, parameter and parameter category to be selected.


### 1.6. System Sequence Diagram (SSD)

Ver docs

### 1.7 Other Relevant Remarks


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

### 2.2. Other Remarks


## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

### Systematization ##


## 3.2. Sequence Diagram (SD)

Ver docs

## 3.3. Class Diagram (CD)

Ver docs

# 4. Tests 


**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Test class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureTestCantHaveNullIsNotAllowed() {
		Test test = new Exemplo(X parameter null);
	}


# 5. Construction (Implementation)


# 6. Integration and Demo 

Needs some modification on the auto generation of the internal code.

# 7. Observations

Needs some modification on the auto generation of the internal code.





