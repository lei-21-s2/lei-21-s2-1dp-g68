# Integrating Project for the 2nd Semester of LEI-ISEP 2020-21 

# 1. Team Members

The teams consists of students identified in the following table. 

| Student Number	| Name |
|--------------|----------------------------|
| **1200619**  | José Lapa                  |
| **1200608**  | Daniel Reis                |
| **1200606**  | António Barros             |
| **1200607**  | Beatriz Lira               |
| **1161274**  | Artur Múria                |




# 2. Task Distribution ###


Throughout the project's development period, the distribution of _tasks / requirements / features_ by the team members was carried out as described in the following table. 

**Keep this table must always up-to-date.**

| Task                      | [Sprint A](SprintA/README.md) | [Sprint B](SprintB/README.md) | [Sprint C](SprintC/README.md) |  [Sprint D](SprintD/README.md) |
|-----------------------------|------------|------------|------------|------------|
| Glossary  |  [all](SprintA/Glossary.md)   |   [all](SprintB/Glossary.md)  |   [all](SprintC/Glossary.md)  | [all](SprintD/Glossary.md)  |
| Use Case Diagram (UCD)  |  [all](SprintA/UCD.md)   |   [all](SprintB/UCD.md)  |   [all](SprintC/UCD.md)  | [all](SprintD/UCD.md)  |
| Supplementary Specification   |  [all](SprintA/FURPS.md)   |   [all](SprintB/FURPS.md)  |   [all](SprintC/FURPS.md)  | [all](SprintD/FURPS.md)  |
| Domain Model  |  [all](SprintA/DM.md)   |   [all](SprintB/DM.md)  |   [all](SprintC/DM.md)  | [all](SprintD/DM.md)  |
| US 3                   |     |  [1200607](SprintB/US3.md) [1161274](SprintB/US3.md)  |   |  |
| US 7                   |     |  [1200606](SprintB/US7.md) [1200608](SprintB/US7.md)  |   |  |
| US 8                   |     |  [1200619](SprintB/US8.md) [1200607](SprintB/US8.md) |   |  |
| US 9                   |     |  [1161274](SprintB/US9.md)  |   |  |
| US 10                  |     |  [1200606](SprintB/US10.md) |   |  |
| US 11                  |     |  [1200619](SprintB/US11.md) |   |  |
