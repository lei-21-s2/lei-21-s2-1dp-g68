# US3 - Register a client

## 1. Requirements Engineering

### 1.1. User Story Description

As a *Receptionist* of the laboratory, I want to register a client.

### 1.2. Customer Specifications and Clarifications 

**From the Specifications Document:** 
- "To register a client, the receptionist needs the client’s citizen card number, National Healthcare Service (NHS) number, birth date, sex,
 Tax Identification number (TIF), phone number, e-mail and name."

**From the client clarifications:**
- Question: What data is needed for a client to book a test ?
    - Answer: The client's citizen card number, the lab order prescribed by a doctor.
- Question: What are the business rules applicable to such data?
    - Answer: ...
    
### 1.3. Acceptance Criteria

- AC1: Citizen card number must be unique and contain 9 ints
- AC2: NHS number must be unique and contain 10 ints
- AC3: TIF number must be unique and contain 10 ints
- AC4: E-mail cannot be empty and has, at maximum, 50 chars
- AC5: Phone number must contain 11 ints
- AC6: Birth date, sex and name is also mandatory
- AC7: The client must be a system user.

### 1.4. Found out Dependencies

No dependencies were found.

### 1.5 Input and Output Data

Input Data
    - **Typed data**: citizen card number, National Healthcare Service (NHS) number, birth date, sex, Tax Identification number (TIF), phone number, e-mail and name
    - **Selected data**: (none)

Output Data
    - (In)Success of the operation.

### 1.6. System Sequence Diagram (SSD)

![US3-SSD](US3-SSD.svg)


### 1.7 Other Relevant Remarks

*Use this section to capture other relevant information that is related with this US such as (i) special requirements ; (ii) data and/or technology variations; (iii) how often this US is held.* 


## 2. OO Analysis

### 2.1. Relevant Domain Model Excerpt 

![US3-MD](US3-MD.svg)

### 2.2. Other Remarks

## 3. Design - User Story Realization 

### 3.1. Rationale

**The rationale grounds on the SSD interactions and the identified input/output data.**

| Interaction ID | Question: Which class is responsible for... | Answer  | Justification (with patterns)  |
|:-------------  |:--------------------- |:------------|:---------------------------- |
| Step 1  		 |							 |             |                              |
| Step 2  		 |							 |             |                              |
| Step 3  		 |							 |             |                              |
| Step 4  		 |							 |             |                              |
| Step 5  		 |							 |             |                              |
| Step 6  		 |							 |             |                              |              

### Systematization ##

According to the taken rationale, the conceptual classes promoted to software classes are: 

 * Class1
 * Class2
 * Class3

Other software classes (i.e. Pure Fabrication) identified: 
 * RegisterClientUI  
 * RegisterClientController

## 3.2. Sequence Diagram (SD)

*In this section, it is suggested to present an UML dynamic view stating the sequence of domain related software objects' interactions that allows to fulfill the requirement.* 

![US3-SD](US3-SD.svg)

## 3.3. Class Diagram (CD)

*In this section, it is suggested to present an UML static view representing the main domain related software classes that are involved in fulfilling the requirement as well as and their relations, attributes and methods.*

![US3-CD](US3-CD.svg)

# 4. Tests 
*In this section, it is suggested to systematize how the tests were designed to allow a correct measurement of requirements fulfilling.* 

**_DO NOT COPY ALL DEVELOPED TESTS HERE_**

**Test 1:** Check that it is not possible to create an instance of the Example class with null values. 

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

*It is also recommended to organize this content by subsections.* 

# 5. Construction (Implementation)

*In this section, it is suggested to provide, if necessary, some evidence that the construction/implementation is in accordance with the previously carried out design. Furthermore, it is recommeded to mention/describe the existence of other relevant (e.g. configuration) files and highlight relevant commits.*

*It is also recommended to organize this content by subsections.* 

# 6. Integration and Demo 

*In this section, it is suggested to describe the efforts made to integrate this functionality with the other features of the system.*


# 7. Observations

*In this section, it is suggested to present a critical perspective on the developed work, pointing, for example, to other alternatives and or future related work.*





